var SHOWOFF = SHOWOFF || {};

(function(ns) {
    'use strict';

    var _images = [];

    var _cfg = {
        'base_url': 'http://visage',
        'album': 'default'
    };

    var init = function(cfg, images) {
        $.extend(_cfg, cfg);
        _images = images;

        //using toggle-publish for publish and disagreed

        $('.toggle-publish').each(function(index, value) {
            var icon = ($(this).data('value') === 'yes') ? 'ok' : 'remove';
            $(this).html(glyph(icon));

        });

        $('.toggle-gender').each(function(index, value) {

            var gender = $(this).data('value')

            var icon = gender;

            $(this).html(font_awesome(icon));

            // put color when it's enabled or not. Need to know if campaign face is the same as selected.

            var faces = $(this).data('faces')
            var faces_label = document.getElementById('faces_campaign');
            var faces_campaign = (faces_label.innerText || faces_label.textContent);

            if (faces_campaign !== faces)
                var gender_color = 'red';
                $(this).css("color", gender_color );
        });

        $('.toggle-faces').each(function(index, value) {

            var faces = $(this).data('value')

            var icon = (faces === 'single')? 'user' : 'users';

            $(this).html(font_awesome(icon));

        });

        $('.gender-record').on('change', function(event) {  //fsanda change this to something that will fire always
            event.preventDefault();
            return gender_record($(this));
        });

        $('.manual_age').on('change', function(event) {  //fsanda change this to something that will fire always
            event.preventDefault();
            return manual_age($(this));
        });

        $('.toggle-publish').on('click', function(event) {
            event.preventDefault();

            var faces = $(this).data('faces');
            var faces_label = document.getElementById('faces_campaign');
            var faces_campaign = (faces_label.innerText || faces_label.textContent);

            if(faces === faces_campaign)
                return toggle_publish($(this)); //python function
            else return false;
        });

        $('.toggle-gender').on('click', function(event) {
            event.preventDefault();

            var faces = $(this).data('faces');
            var faces_label = document.getElementById('faces_campaign');
            var faces_campaign = (faces_label.innerText || faces_label.textContent);

            if(faces === faces_campaign)
                return toggle_gender($(this)); //python function
            else
                return false;
        });

        $('.toggle-faces').on('click', function(event) {
            event.preventDefault();
            return toggle_faces($(this)); //python function
        });

        $('.toggle-all').each(function() {
            var icon = ($(this).data('value') === 'yes') ? 'ok' : 'remove';
            $(this).html(glyph(icon));
        });

        $('.toggle-all').on('click', function(event) {
            event.preventDefault();
            return toggle_all($(this)); //python function
        });

        $('.extra').on('click', function(event) {
            event.preventDefault();
            return toggle_gender($(this));
        });

        $('.toggle-bool').each(function() {
            var icon = ($(this).data('value') === 'yes') ? 'ok' : 'remove';
            $(this).html(glyph(icon));
        });

        $('.toggle-bool').on('click', function(event) {
            event.preventDefault();
            return toggle_bool($(this));
        });
    };

    var get = function(key) {
        return _cfg[key];
    };

    var set = function(key, value) {
        _cfg[key] = value;
    };

    var album_url = function() {
        //return ns.get('base_url') + ns.get('album') + '/';
        return ns.get('base_url');
        //return ns.get('base_url') + ns.get('album') + ns.get('type_faces') + '/';
    };

    var file_url = function(filename) {
        return album_url() + filename + '/';
    };

    var glyph = function(icon) {
        return '<span class="glyphicon glyphicon-' + icon + '"></span>';
    };

    var font_awesome = function (icon) {
        return '<span class="fa fa-' + icon + '"></span>';
    };

    var toggle = function(element, url) {

        $(element).html('please wait ...');
        var new_value = (element.data('value') === 'yes') ? 'no' : 'yes';

        $.getJSON(url, false, function() {});

        var icon = (new_value === 'yes') ? 'ok' : 'remove';
        var new_text = glyph(icon);

        $(element).data('value', new_value);
        $(element).html(new_text);
        $(element).attr('data-value',new_value);

        return new_value;

    };

    var toggle_node = function(node, url, state) {

        $.getJSON(url, false, function() {});

        var icon = (state === 'yes') ? 'ok' : 'remove';
            var new_text = glyph(icon);

            $(node).data('value', state);
            $(node).html(new_text);
            $(node).attr('data-value',state);

            var filename = $(node).data('filename')

            if (state === 'no')
                document.getElementById('div_' + filename ).classList.remove('published')
            else
                document.getElementById('div_' + filename ).classList.add('published')
    };

    var toggle_single = function(element) {

        var new_value = (element.data('value') === 'yes') ? 'no' : 'yes';
        var icon = (new_value === 'yes') ? 'ok' : 'remove';
        var new_text = glyph(icon);

         $(element).data('value', new_value);
         $(element).html(new_text);
    };

    var toggle_all = function(element) {

        var image_frame = document.getElementsByClassName('toggle-publish');

        toggle_single(element);

        var state_publish = element.data('value');

        var path = window.location.pathname;
        var toggle_function_url = '';

        if(path.includes('/gallery/campaign/'))
            toggle_function_url = 'toggle_publish_all';
        else
            if(path.includes('/gallery/disagreed/'))
                toggle_function_url = 'toggle_resolve_each';

        for(var i = 0; i < image_frame.length; i++)
        {
            var url = file_url(image_frame.item(i).getAttribute('data-filename')) + toggle_function_url + '/' + state_publish;

            //if the image matches the campaign face:

            var image_type_faces = image_frame.item(i).getAttribute('data-faces');
            var faces_label = document.getElementById('faces_campaign');
            var faces_campaign = (faces_label.innerText || faces_label.textContent);

            if(faces_campaign === image_type_faces || state_publish === 'no')
                toggle_node(image_frame.item(i), url, state_publish);
        }
    };

    var toggle_publish = function(element) {

        var filename = element.data('filename');
        var path = window.location.pathname;
        var toggle_function_url = '';

        if(path.includes('/gallery/campaign/'))
            toggle_function_url = 'toggle_publish';
        else
            if(path.includes('/gallery/disagreed/'))
                toggle_function_url = 'toggle_resolve';


        var url = file_url(filename) + toggle_function_url;

        var new_value = toggle(element, url);

        if (new_value === 'no') {
            document.getElementById('div_' + filename ).classList.remove('published')
        } else {
            document.getElementById('div_' + filename).classList.add('published')

        }
    };

    var toggle_gender = function(element) {

        var filename = element.data('filename');

        var gender = element.data('value');

        var new_value = (gender === 'male') ? 'female' : 'male';

        var path = window.location.pathname;
        var gender_function_url = '';

        if(path.includes('/gallery/campaign/'))
            gender_function_url = '/manual_gender/';
        else
            if(path.includes('/gallery/disagreed/'))
                gender_function_url = '/toggle_gender/';
        else
            if(path.includes('/gallery/gender_inaccurate/'))
                gender_function_url = '/toggle_gender_inaccurate';

        // var url =  file_url(filename) + new_value + gender_function_url;
        var url =  file_url(filename) + gender + gender_function_url;

        console.log(url);

        //$(element).html('please wait ...');

        $.getJSON(url, false, function() {

            var new_text = font_awesome(new_value);

            $(element).data('value', new_value);
            $(element).html(new_text);
            $(element).attr('data-value', new_value)
        });

        //if the page is list then do the rest

        if(path.includes('/gallery/campaign/'))
        {
            var node_state = document.getElementById(filename);
            var icon = 'ok';
            var new_text = glyph(icon);

            $(node_state).data('value', 'yes');
            $(node_state).html(new_text);

            document.getElementById('div_' + filename).classList.add('published')
            $(node_state).attr('data-value','yes')
        }
    };

    var toggle_faces = function(element) {

        var filename = element.data('filename');

        var type_faces = element.data('value');

        var new_value = (type_faces === 'single') ? 'multiple' : 'single';

        //$(element).html('please wait ...');

        var url_faces =  file_url(filename) + new_value + '/manual_faces/';

        $.getJSON(url_faces, false, function() {

        });

        var icon = (new_value === 'single'? 'user':'users');
        var new_text = font_awesome(icon);

        $(element).data('value', new_value);
        $(element).html(new_text);
        $(element).attr('data-value', new_value);

        // if faces_label different than campaign, press 'x'

        var faces_label = document.getElementById('faces_campaign');
        var faces_campaign = (faces_label.innerText || faces_label.textContent);
        var node_state = document.getElementById(filename);
        var node_gender = document.getElementById('btn_gender_' + filename);

        var url_toggle = file_url(filename) + 'toggle_publish';

        $(node_state).attr('data-faces', new_value);

        if(faces_campaign !== new_value){

            toggle_node(node_state, url_toggle, 'no');

            // disable button so it can't be casted and give a hint

            //$('.gender-record').css('color', 'yellow')

            $(node_gender).css("color", "red" );
            $(node_state).off();
            $(node_gender).off();

        }
        else{
            toggle_node(node_state, url_toggle, 'yes');

            var color_gender = 'blue';

            $(node_gender).css("color", color_gender );

            $(node_state).on('click', function(event) {
                event.preventDefault();
                return toggle_publish($(this)); //python function
        });

            $(node_gender).on('click', function(event) {
                event.preventDefault();
                return toggle_gender($(this)); //python function
        });
        }
    };

    var gender_record = function(element) {

        var filename = element.data('filename');

        var strID = 'ddl_gender_' + filename;

        var gender = document.getElementById(strID).value;

        var path = window.location.pathname;
        var gender_function_url = '';

        if(path.includes('/gallery/campaign/'))
            gender_function_url = '/manual_gender/';
        else
            if(path.includes('/gallery/disagreed/'))
                gender_function_url = '/resolve_gender/';

        var url =  file_url(filename) + gender + gender_function_url;

        $.getJSON(url, false, function() {

        });
    };

    var manual_age = function(element) {

        var strID = 'manual_age_' + element.data('filename');

        var age = document.getElementById(strID).value;

        var url =  file_url(element.data('filename'))  + age + '/manual_age/' ;

        $.getJSON(url, false, function() {

        });
    };

    var toggle_bool = function(element) {
        var new_value = (element.data('value') === 'yes') ? 'no' : 'yes';
        var url = album_url() + 'set/' + element.data('setting')  + '/' + new_value;
        toggle(element, url);
    };

    // export public functions
    ns.init = init;
    ns.get = get;
    ns.set = set;
}(SHOWOFF));