#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Showoff - Webbased photo album software

Copyright (c) 2010-2014 by Jochem Kossen <jochem@jkossen.nl>

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above
   copyright notice, this list of conditions and the following
   disclaimer in the documentation and/or other materials provided
   with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import json
import os
import re

from flask import Blueprint, current_app, send_from_directory, request, redirect, session, url_for, jsonify, flash, \
    render_template, send_file, abort

import config
from scripts.visage_scripts import update_user_values
from visage.lib import Show, Image, CacheManager
from visage.lib.campaign import model_campaign
from visage.lib.campaign.model_campaign import Campaign, get_campaigns, get_all_campaigns, get_campaign_id, \
    update_pending_table_campaign
from visage.lib.database_manager import prepare_data
from visage.lib.deleted import model_deleted
from visage.lib.photo import model_photo
from visage.lib.photo.model_photo import Photo
from visage.lib.resolve import model_resolve
from visage.lib.resolve.model_resolve import Resolve
from visage.lib.state.model_state import State
from visage.lib.statistics.model_statistics import Statistics, get_votes_description
from visage.lib.user.model_user import User
from visage.lib.vote.model_vote import Vote
import zipfile

from visage_tools import fix_downloads, fix_image, get_rectangle_dlib

frontend = Blueprint('frontend', __name__, template_folder='templates')

service_method = 'azure'


@frontend.route('/login', methods=['POST', 'GET'])
def login():
    error = None

    if request.method == 'POST':

        username = request.form['inputUsername']
        password = request.form['inputPassword']

        user = User(username, password)

        if user.authenticate_user():
            session['logged_in'] = True
            session['inputUsername'] = request.form['inputUsername']
            flash('You were successfully logged in')
            return redirect(url_for('frontend.index'))
        else:
            error = 'Invalid credentials'

    return render_template('login.html', error=error)


@frontend.route('/register', methods=['POST', 'GET'])
def register():
    error = None

    if request.method == 'POST':

        username = request.form['inputUsername']
        email = request.form['inputEmail']
        password = request.form['inputPassword']

        user = User(username, password)
        user.set_email(email)

        if user.register_user():
            session['inputUsername'] = request.form['inputUsername']
            message = 'registered successfully'
            flash(message)
            return redirect(url_for('frontend.index'))

        else:
            error = 'email/username already in use'
            # return message

    return render_template('register.html', error=error)


@frontend.route('/static_files/<path:filename>')
def static_files(filename):
    """Send static files such as style sheets, JavaScript, etc."""
    static_path = os.path.join(frontend.root_path, 'static')
    return send_from_directory(static_path, filename)


@frontend.route('/image/<album>/<filename>/full/', defaults={'size': 'full'})
@frontend.route('/image/<album>/<filename>/<int:size>/')
def get_image(album, filename, size="full"):
    image = Image(album, filename, current_app.config, service_method)
    cache = CacheManager(image, current_app.config)

    cache_path = os.path.expanduser(str(cache.get_path(size)))

    if os.path.exists(cache_path):
        return send_from_directory(*cache.get(size))
    else:
        cache_path_full = os.path.expanduser(str(cache.get_path('full')))
        if os.path.exists(cache_path_full):
            return send_from_directory(*cache.get(size))
        else:
            return 'file not found: {0}'.format(cache_path)

        # fsanda


@frontend.route('/image_face/<album>/<filename>')
def get_image_face(album, filename):
    from io import BytesIO
    from PIL import Image as PilImage, ImageDraw

    # fix image

    if config.FIX_IMAGES:
        fix_image(filename)

    path_file = os.path.join(config.ALBUMS_DIR, album, filename)

    if os.path.exists(path_file):

        image = Image(album, filename, current_app.config, service_method)

        list_coordinates_rectangle = image.get_face_rectangle_coordinates()

        im = PilImage.open(os.path.join(config.ALBUMS_DIR, album, filename))

        '''
        
        for orientation in ExifTags.TAGS.keys():
            if ExifTags.TAGS[orientation]=='Orientation':
                break

        exif = dict(im._getexif().items())

        if exif[orientation] == 3:
            im = im.rotate(180, expand=True)
        elif exif[orientation] == 6:
            im = im.rotate(270, expand=True)
        elif exif[orientation] == 8:
            im = im.rotate(90, expand=True)
        im.save(path_file)
        im.close()
        
        '''

        draw = ImageDraw.Draw(im)

        if list_coordinates_rectangle is not None:

            for rec in list_coordinates_rectangle:
                draw.rectangle(xy=rec, outline='green')

        del draw

        byte_io = BytesIO()
        im.save(byte_io, 'PNG')
        byte_io.seek(0)

        return send_file(byte_io, mimetype='image/png')

    else:
        print('file not found')

        # create file not found image

        # byte_io = BytesIO()
        # im.save(byte_io, 'PNG')
        # byte_io.seek(0)

        return 'file not found'


@frontend.route('/<campaign_id>/<filename>.html/<gender_edit>')
def image_page(campaign_id, filename, gender_edit):
    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired'

    if model_deleted.check_deleted(os.path.splitext(filename)[0]):
        return jsonify(result='Image was deleted')

    campaign = Campaign(campaign_id=campaign_id)

    album = campaign.get_age()

    image = Image(album, filename, current_app.config, service_method)

    show = Show(user=user, campaign=campaign)

    # get tag image from database with filename

    # get url from database with filename

    title = image.get_title()

    description = image.get_description()

    tag_list = image.get_tags()

    url = image.get_url()

    s_edit_gender = str(gender_edit)

    edit_gender = True if s_edit_gender == 'True' else False

    # if image is deleted direct to page deletion

    return render_template(
        'image.html',
        album=album,
        filename=filename,
        show=show,
        title=title,
        description=description,
        tags=tag_list,
        url=url,
        date_metadata=image.get_date(),
        gender_edit=edit_gender
    )


@frontend.route('/list/<campaign_id>.html')
def list_album(campaign_id):
    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired: cannot retrieve user'

    campaign = Campaign(campaign_id=campaign_id)

    age = campaign.get_age()

    if str.isdigit(str(age)):
        # control session expiry

        show = Show(user=user, campaign=campaign)
        show.set_method(service_method)

        new_gender = 'male' if str(campaign.get_gender()) == 'female' else 'female'

        new_campaign = Campaign(age=campaign.get_age(), gender=new_gender, type_faces='single')
        new_campaign.set_id()

        show.load(config.THUMBNAILS_PER_PAGE)

        data = show.get_data()

        dict_data = dict(data.get('dict_data'))

        ext = re.compile(".(jpg|png|gif|bmp)$", re.IGNORECASE)
        album_dir = os.path.join(current_app.config['ALBUMS_DIR'], str(age))

        all_files = [f for f in os.listdir(album_dir) if ext.search(f)]
        all_files.sort()

        # shuffled_files = random.sample(dict_data.keys(), len(dict_data))
        # files = shuffled_files

        files = dict_data

        completed = 0 if session.get('completed') is None else session.get('completed')

        return render_template(
            'list.html',
            show=show,
            files=files,
            dictionary_data=dict_data, completed=completed)


# shows first page of all images (we will show all)
@frontend.route('/campaign/<campaign_id>.html')
def show_album(campaign_id):
    """Render first page of album"""

    return list_album(campaign_id)


@frontend.route('/')
def index():
    latest_campaigns = None

    if session.get('logged_in'):
        latest_campaigns = get_campaigns(config.LATEST_CAMPAIGN_SIZE)

    return render_template('index.html', campaigns=latest_campaigns)


# shows cover of album gallery
@frontend.route('/campaign/<campaign_id>', methods=['GET', 'POST'])
def show_album_gallery(campaign_id=None):
    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'session expired'  # todo send to error page

    if session.get('completed'):
        session.pop('completed')

    age = gender = type_faces = campaign = None

    if request.method == 'GET':
        campaign = Campaign(campaign_id=campaign_id)

        age = campaign.get_age()

    elif request.method == 'POST':

        if "campaign_age" in request.form:
            campaign_age = str(request.form["campaign_age"])
            age = campaign_age

        if "campaign_gender" in request.form:
            campaign_gender = str(request.form["campaign_gender"])
            gender = campaign_gender

        if "campaign_type_faces" in request.form:
            campaign_type_faces = str(request.form["campaign_type_faces"])
            type_faces = campaign_type_faces

        campaign = Campaign(age=age, gender=gender, type_faces=type_faces)
        campaign.set_id()

    dir_list = os.listdir(current_app.config['ALBUMS_DIR'])

    # get single, multiple or other according to option chosen by user

    show = None

    if str(age) in dir_list:
        campaign.set_last_access()
        show = Show(user=user, campaign=campaign)
        show.set_method(service_method)

        show.load(1)

        session['total_images'] = show.nr_of_items_db

        # update pending db

        update_pending_table_campaign(number_images=show.nr_of_items_db, user=user, campaign_id=campaign.get_id())

    if not show:
        message = 'no images found'
        flash(message)
        campaigns_latest = get_campaigns(config.LATEST_CAMPAIGN_SIZE)
        return render_template('index.html', campaigns=campaigns_latest)

    return render_template('album.html', show=show, completed=0)


@frontend.route('/<album>/')
def toggle_publish(album):
    print(album)
    return jsonify(result='OK')


@frontend.route('/<album>/')
def toggle_resolve(album):
    print('here resolve', album)
    return jsonify(result='OK')


@frontend.route('/<campaign_id>/<filename>/toggle_publish/')
def toggle_publish_image(campaign_id, filename):
    """Toggle publish image"""

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'session expired'  # todo send to error page

    campaign = Campaign(campaign_id=campaign_id)

    try:
        # insert record on database vote, sum if it already exists

        state = State(user=user, campaign=campaign)

        state.toggle_state_publish(filename)

    except Exception as ex:
        print(ex)
        return jsonify(result='Failed')

    return jsonify(result='OK')


@frontend.route('/<campaign_id>/<filename>/toggle_resolve/')
def toggle_resolve_image(campaign_id, filename):
    """Toggle resolve image"""

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'session expired'  # todo send to error page

    campaign = Campaign(campaign_id=campaign_id)

    try:

        # insert record onto toggle resolution

        resolve_disagreed = Resolve(campaign=campaign, user=user)

        resolve_disagreed.toggle(filename, 'state')

    except Exception as ex:
        print(ex)
        return jsonify(result='Failed')

    return jsonify(result='OK')


@frontend.route('/<campaign_id>/<filename>/toggle_publish_all/<state_publish>')
def toggle_publish_all(campaign_id, filename, state_publish):
    """Toggle publish image"""

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'session expired'  # todo send to error page

    campaign = Campaign(campaign_id=campaign_id)

    try:
        # insert record on database vote, sum if it already exists

        state = State(user=user, campaign=campaign)

        state.toggle_state_all(filename, state_publish)

    except Exception as ex:
        print(ex)
        return jsonify(result='Failed')

    return jsonify(result='OK')


@frontend.route('/<campaign_id>/<filename>/toggle_resolve_each/<state_publish>')
def toggle_resolve_each(campaign_id, filename, state_publish):
    """Toggle resolution of each image"""

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'session expired'  # todo send to error page

    campaign = Campaign(campaign_id=campaign_id)

    try:

        resolve_disagreed = Resolve(campaign=campaign, user=user)

        resolve_disagreed.toggle_each(filename, state_publish)

    except Exception as ex:
        print(ex)
        return jsonify(result='Failed')

    return jsonify(result='OK')


@frontend.route('/<campaign_id>/<filename>/<gender>/manual_gender/')
def gender_record_image(campaign_id, filename, gender):
    """Toggle publish image"""

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'session expired'  # todo send to error page

    try:

        campaign = Campaign(campaign_id=campaign_id)

        state = State(user=user, campaign=campaign)

        # get inverse gender

        inverse_gender = model_campaign.get_inverse_gender(gender)

        state.record_gender(filename, inverse_gender)

    except Exception as ex:
        print(ex)
        return jsonify(result='Failed')

    return jsonify(result='OK')


@frontend.route('/<campaign_id>/<filename>/<gender>/toggle_resolve_gender/')
def toggle_resolve_gender(campaign_id, filename, gender):
    """Toggle resolve image"""

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'session expired'  # todo send to error page

    try:

        campaign = Campaign(campaign_id=campaign_id)

        resolve_disagreed = Resolve(campaign=campaign, user=user)

        resolve_disagreed.toggle(filename, gender)

    except Exception as ex:
        print(ex)
        return jsonify(result='Failed')

    return jsonify(result='OK')


@frontend.route('/<campaign_id>/<filename>/<gender>/toggle_gender_inaccurate/')
def toggle_gender_inaccurate_image(campaign_id, filename, gender):
    """Toggle resolve gender inaccurate"""

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'session expired'  # todo send to error page

    try:

        campaign = Campaign(campaign_id=campaign_id)

        resolve_gender = Resolve(campaign=campaign, user=user)

        resolve_gender.toggle_gender_inaccurate(filename, gender)

    except Exception as ex:
        print(ex)
        return jsonify(result='Failed')

    return jsonify(result='OK')


@frontend.route('/<campaign_id>/<filename>/<type_faces>/manual_faces/')
def faces_record_image(campaign_id, filename, type_faces):
    """Toggle publish image"""

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'session expired'  # todo send to error page

    try:

        campaign = Campaign(campaign_id=campaign_id)

        state = State(user=user, campaign=campaign)

        state.record_type_faces(filename, type_faces)

    except Exception as ex:
        print(ex)
        return jsonify(result='Failed')

    return jsonify(result='OK')


@frontend.route('/<campaign_id>/<filename>/<age>/manual_age/')
def age_record_image(campaign_id, filename, age):
    """Toggle publish image"""

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'session expired'  # todo send to error page

    try:

        campaign = Campaign(campaign_id=campaign_id)

        state = State(user=user, campaign=campaign)

        state.record_age(filename, age)

    except Exception as ex:
        print(ex)
        return jsonify(result='Failed')

    return jsonify(result='OK')


@frontend.route('/vote/<campaign_id>/<completed>')
def vote(campaign_id, completed):
    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired'

    session['completed'] = completed

    campaign = Campaign(campaign_id=campaign_id)

    v = Vote(user, campaign)

    v.submit_vote()

    # todo check amount of images if none left, present a different message

    message = 'Thank you for your vote! next set of images are presented'

    flash(message)

    # load new images

    return redirect(url_for('frontend.show_album', campaign_id=campaign.get_id()))


@frontend.route('/statistics/<statistics_type>/')
def statistics(statistics_type):
    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired'

    stats = Statistics(user)

    if statistics_type == 'general':
        return render_template('statistics_general.html', statistics=stats)

    if statistics_type == 'general_per_age':
        dict_table, dict_sum = stats.get_table_general()
        return render_template('statistics_general_per_age.html', statistics=stats, table=dict_table, sums=dict_sum)
    elif statistics_type == 'missing':
        dict_table, dict_sum = stats.get_table_missing()
        return render_template('statistics_missing.html', statistics=stats, table=dict_table, sums=dict_sum)
    elif statistics_type == 'disagreed':
        dict_table, dict_sum = stats.get_table_disagreed()
        return render_template('statistics_disagreed.html', statistics=stats, table=dict_table, sums=dict_sum)
    elif statistics_type == 'age_range':
        dict_table, dict_sum = stats.get_table()
        return render_template('statistics_age_range.html', statistics=stats, table=dict_table, sums=dict_sum)
    elif statistics_type == 'users':
        return render_template('statistics_users.html', statistics=stats)
    elif statistics_type == 'gender_inaccurate':
        dict_table, dict_sum = stats.get_table_gender_inaccurate()
        return render_template('statistics_gender_inaccurate.html', statistics=stats, table=dict_table, sums=dict_sum)
    else:
        return 'error with statistics_type', statistics_type


@frontend.route('/statistics_positives/<votes>/<my_vote>')
@frontend.route('/statistics_positives/<votes>/')
def statistics_positives(votes, my_vote=None):
    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired'

    stats = Statistics(user)

    if my_vote is None:
        user_check = False
        dict_table, dict_sum = stats.get_table_positives(votes, user_check)
    else:
        user_check = True
        dict_table, dict_sum = stats.get_table_positives(votes, user_check)

    field_description = get_votes_description(votes)

    i_votes = int(votes)

    return render_template(
        'statistics_positives.html',
        statistics=stats,
        table=dict_table,
        sums=dict_sum,
        votes=i_votes,
        votes_description=field_description,
        my_vote=my_vote
    )


@frontend.route('/campaigns/')
def campaigns():
    if session.get('inputUsername'):
        user = str(session['inputUsername'])
        pass
    else:
        return 'Session expired'

    campaign_list = get_all_campaigns()

    # for each campaign, get number of images left

    for c in campaign_list:
        campaign_id = c.get('_id')

        campaign = Campaign(campaign_id=campaign_id)

        campaign.set_user(user=user)

        number_images = campaign.get_pending_campaign_number()

        c.update({
            'number_images': number_images
        })

    return render_template('campaigns.html', campaigns=campaign_list)


@frontend.route('/positive/<age>/<gender>/<votes>/<my_vote>')
@frontend.route('/positive/<age>/<gender>/<votes>')
def positive(age, gender, votes, my_vote=None):
    # todo implemented only for single faces

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired'

    campaign = Campaign(age=age, gender=gender, type_faces='single')
    campaign.set_user(user)
    campaign.set_id()

    i_votes = int(votes)

    if my_vote is None:

        edit_gender = True if i_votes == config.NUMBER_VOTES else False

        return render_template(
            'positive.html',
            campaign=campaign,
            votes=i_votes,
            user_check=False,
            gender_edit=edit_gender
        )
    else:
        return render_template('positive.html', campaign=campaign, votes=i_votes, user_check=True, gender_edit=False)


@frontend.route('/positive_resolved/<age>/<gender>/<votes>/')
@frontend.route('/positive_resolved/<age>/<gender>/<votes>/<my_vote>/')
def positive_resolved(age, gender, votes, my_vote=None):
    # todo implemented only for single faces

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired'

    campaign = Campaign(age=age, gender=gender, type_faces='single')
    campaign.set_id()
    campaign.set_user(user)

    campaign_male = Campaign(age=age, gender='male', type_faces='single')
    campaign_male.set_id()
    campaign_male.set_user(user)

    campaign_female = Campaign(age=age, gender='female', type_faces='single')
    campaign_female.set_id()
    campaign_female.set_user(user)

    # get list from male and female while resolved

    resolve_male = Resolve(campaign=campaign_male, user=user)
    resolve_female = Resolve(campaign=campaign_female, user=user)

    list_file_objects_male = resolve_male.get_positive_resolved(gender)
    list_file_objects_female = resolve_female.get_positive_resolved(gender)

    list_file_objects = list(set(list_file_objects_male + list_file_objects_female))

    list_files = [str(x) + config.IMAGE_EXTENSION for x in list_file_objects]

    i_votes = int(votes)

    if my_vote is None:
        return render_template(
            'positive_resolved.html',
            campaign=campaign,
            resolved=list_files,
            votes=i_votes,
            user_check=False
        )
    else:
        return render_template('positive_resolved.html', campaign=campaign, votes=i_votes, user_check=True)


@frontend.route('/rotate/<album>/<filename>/')
def rotate(album, filename):
    # open file from server,

    # rotate and save, plus face detection with dlib

    from PIL import Image as PilImage

    image_path = os.path.join(config.ALBUMS_DIR, album, filename)

    im = PilImage.open(image_path)

    rotated = im.rotate(90)

    rotated.save(image_path)

    rotated.close()

    get_rectangle_dlib(album, filename)

    return jsonify(result='Image rotated... please press back and refresh the main page')


@frontend.route('/gender/<filename>/')
def change_gender(filename):
    file_without_extension = str(os.path.splitext(filename)[0])

    gender_inverse = model_campaign.change_gender(file_without_extension)

    if gender_inverse is not None:
        # check if filename is resolved if it is, change it there

        model_resolve.change_resolved(file_without_extension, gender_inverse)

        # delete if filename is in campaign_transit

        model_campaign.delete_campaign_transition(file_without_extension)

        return jsonify(result='Gender Updated... please refresh the main page')
    return jsonify(result='Gender not Updated because 3 votes are needed')


@frontend.route('/delete/<filename>/')
def delete_image(filename):

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired'

    file_without_extension = str(os.path.splitext(filename)[0])

    model_deleted.delete_item(file_without_extension, user=user)

    return jsonify(result='File Deleted... please refresh the main page')


@frontend.route('/disagreed/<age>/<gender>/')
def show_disagreed(age, gender):
    tuple_disagreed_data = ()

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired'

    campaign = Campaign(age=age, gender=gender, type_faces='single')
    campaign.set_id()

    # populate toggle tables

    disagreed_files = campaign.get_disagreed_files()

    if len(disagreed_files) > 0:
        resolved_records = Resolve(campaign, user)

        resolved_records.insert_toggle_resolution(disagreed_files)

        # get records with state

        tuple_disagreed_data = resolved_records.get_disagreed_data()

    return render_template('disagreed.html', campaign=campaign, tuple_disagreed_files=tuple_disagreed_data)


@frontend.route('/gender_inaccurate/<age>/<gender>/')
def show_gender_inaccurate(age, gender):
    tuple_gender_inaccurate_data = ()

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired'

    campaign = Campaign(age=age, gender=gender, type_faces='single')
    campaign.set_id()

    stats = Statistics(user)

    # get first 30 images

    list_file_objects = stats.get_votes_gender_inconsistent(
        number_votes=config.NUMBER_VOTES,
        campaign=campaign
    )[:config.THUMBNAILS_PER_PAGE]

    list_files = [str(x) + config.IMAGE_EXTENSION for x in list_file_objects]

    if len(list_files) > 0:
        resolved_records = Resolve(campaign, user)

        resolved_records.insert_toggle_resolution_gender(list_files)

        tuple_gender_inaccurate_data = resolved_records.get_gender_inaccurate_data()

    # create tuple_gender_inaccurate_data

    return render_template(
        'gender_inaccurate.html',
        campaign=campaign,
        tuple_gender_inaccurate_files=tuple_gender_inaccurate_data
    )


@frontend.route('/resolve/<campaign_id>/')
def resolve(campaign_id):
    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired'

    campaign = Campaign(campaign_id=campaign_id)

    resolve_disagreed = Resolve(campaign, user)

    count_disagreed = resolve_disagreed.submit_resolution()

    # todo check amount of images if none left, present a different message

    if count_disagreed is not None:

        if count_disagreed != 0:

            message = 'Thank you for your resolution! next set of images are presented'

        else:

            message = 'Thank you for your resolution!'
    else:
        message = 'Null value'

    flash(message)

    return redirect(url_for('frontend.show_disagreed', age=campaign.get_age(), gender=campaign.get_gender()))


@frontend.route('/<campaign_id>/<filename>/<gender>/toggle_gender/')
def toggle_gender(campaign_id, filename, gender):
    """Toggle resolve image"""

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'session expired'  # todo send to error page

    try:

        campaign = Campaign(campaign_id=campaign_id)

        resolve_disagreed = Resolve(campaign=campaign, user=user)

        resolve_disagreed.toggle_gender(filename, gender)

    except Exception as ex:
        print(ex)
        return jsonify(result='Failed')

    return jsonify(result='OK')


@frontend.route('/resolve_gender_inaccurate/<campaign_id>/')
def resolve_gender_inaccurate(campaign_id):
    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired'

    campaign = Campaign(campaign_id=campaign_id)

    resolve_gender = Resolve(campaign, user)

    count_gender_inaccurate = resolve_gender.submit_resolution_gender()

    # todo check amount of images if none left, present a different message

    if count_gender_inaccurate is not None:

        if count_gender_inaccurate != 0:

            message = 'Thank you for your resolution! next set of images are presented'

        else:

            message = 'Thank you for your resolution!'
    else:
        message = 'None value'

    flash(message)

    return redirect(url_for('frontend.show_gender_inaccurate', age=campaign.get_age(), gender=campaign.get_gender()))


@frontend.route('/download/<age>/<gender>/<user_check>/<votes>')
def download(age, gender, user_check, votes):
    print('downloading...')

    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired'

    campaign = Campaign(age=age, gender=gender, type_faces='single')
    campaign.set_id()
    campaign.set_user(user)

    # get approved files

    s_user_check = str(user_check)
    i_votes = int(votes)

    if s_user_check == 'True':
        list_files = campaign.get_positive_files(votes=i_votes, user_check=True)
    elif s_user_check == 'False':
        list_files = campaign.get_positive_files(votes=i_votes, user_check=False)
    else:
        return 'error with value ' + s_user_check

    attachment_file = str(campaign.get_id()) + '.zip'
    metadata_filename = str(campaign.get_id()) + '.json'
    zip_directory = os.path.expanduser(config.ZIP_PATH)
    zip_path = os.path.join(zip_directory, attachment_file)
    metadata_path = os.path.join(zip_directory, metadata_filename)

    list_dict_image = []

    try:

        for f in list_files:
            dict_image_info = prepare_data(f)

            list_dict_image.append(dict_image_info)

        with open(metadata_path, 'w') as mf:
            # mf.write(json.dumps(unicode(list_dict_image), ensure_ascii=False))
            mf.write(json.dumps(list_dict_image))

        print('metadata file created')

        with zipfile.ZipFile(zip_path, "w", zipfile.ZIP_DEFLATED, allowZip64=True) as zf:

            for f in list_files:

                # do the automatic process to get the highest resolution image

                if config.FIX_IMAGES:
                    fix_image(f)

                image_path = os.path.join(config.ALBUMS_DIR, str(campaign.get_age()), f)

                if os.path.exists(image_path):
                    zf.write(image_path, str(f))

                else:
                    print('path not available')

            # write to a file the metadata

            zf.write(metadata_path, metadata_filename)

        return send_from_directory(
            zip_directory,
            attachment_file, as_attachment=True)

    except Exception as e:
        return str(e)


@frontend.route('/campaign_description/<campaign_id>')
def campaign_description(campaign_id):
    campaign = Campaign(campaign_id=campaign_id)

    return render_template('campaign.html', campaign=campaign)


@frontend.route('/search/')
def search():
    if session.get('inputUsername') is None:
        return 'Session expired'

    return render_template('search.html')


@frontend.route('/search_id/', methods=['POST'])
def search_id():
    if session.get('inputUsername') is None:
        return 'Session expired'

    if request.method == 'POST':
        sid = request.form['search_id']

        campaign_id = get_campaign_id(sid)

        if campaign_id is not None:
            return image_page(campaign_id, sid + config.IMAGE_EXTENSION, True)
        else:
            # try on other images

            return 'image id not found'


@frontend.route('/logout/')
def logout():
    session.clear()

    flash('You have been logged out')

    return redirect(url_for('frontend.index'))


@frontend.route('/update_number_images/')
def update_number_images():
    if session.get('inputUsername'):
        user = str(session['inputUsername'])
    else:
        return 'Session expired'

    update_user_values(user)

    return campaigns()


# ------------------------------

# web services

# make a service that returns json from image id


@frontend.route('/api/v1.0/image/<string:image_id>', methods=['GET'])
def get_task(image_id):
    # get image

    photo = Photo(image_id)

    photo_details = photo.get_info()

    if photo_details is None:
        abort(404)

    return jsonify(photo_details)


@frontend.route('/api/v1.0/get_image_data/<int:face_rectangles>/<int:unique>/', methods=['GET'])
def get_image_data(face_rectangles, unique):
    # do fix first

    if config.FIX_IMAGES:
        fix_downloads()

    data = model_photo.get_data(rectangles=bool(face_rectangles), unique=bool(unique))

    if data is None:
        abort(404)

    resp = jsonify(data)
    resp.status_code = 200

    return resp
