from flask import abort, session

from visage.lib.database_manager import get_data, number_items
from visage.lib.vote.model_vote import Vote


def get_paginator(campaign):

    user = session['inputUsername']

    vote = Vote(user=user, campaign=campaign)
    voted_user = vote.filter_voted()
    voted_all = vote.filter_voted_all()
    voted = voted_user + voted_all

    num_item = number_items(voted_items=voted, campaign=campaign)

    if num_item == 0:
        print('fsanda no files created in db')
        abort(404)

    data = get_data(voted_items=voted, user=user, number_images=num_item, campaign=campaign)

    # try send list in random order

    try:
        return data

    except Exception as ex:
        print(ex)
        abort(404)
