import os
from bson import ObjectId
import config
from db_config import client_db
from visage.lib.deleted import model_deleted

cl_resolve = client_db["YAHOO_REST"]["resolve"]
cl_toggle_resolution = client_db["YAHOO_REST"]["toggle_resolution"]
cl_toggle_resolution_gender = client_db["YAHOO_REST"]["toggle_resolution_gender"]


class Resolve:
    def __init__(self, campaign, user):
        self.campaign = campaign
        self.user = user
        self.client_resolve = cl_resolve

    def get_campaign(self):
        return self.campaign

    def get_user(self):
        return self.user

    def get_state_resolution(self):
        cursor = cl_toggle_resolution.find({
            'campaign_id': self.get_campaign().get_id()
        })

        return cursor

    def get_client_resolve(self):
        return self.client_resolve

    def submit_resolution(self):

        # get all records corresponding to campaign on the toggle table

        cursor = cl_toggle_resolution.find(
            {
                'campaign_id': self.get_campaign().get_id(),
            }
        )

        file_list_delete = []

        for rec in cursor:

            file_id = rec.get('id')
            state = rec.get('state')

            pipe = {
                'id': file_id,
                'campaign_id': self.get_campaign().get_id(),
                'gender': rec.get('gender'),
                'state': state,
                'age_campaign': self.get_campaign().get_age()
            }

            if self.get_client_resolve().find_one(pipe):
                print('image has already been resolved', pipe)

            else:

                # insert record to new resolve table

                pipe.update({
                    'user': self.get_user()
                })

                self.get_client_resolve().insert(pipe)

                # add item to a list to be deleted

                file_list_delete.append(file_id)

        # delete records

        if len(file_list_delete) != 0:
            count_records = self.delete_records(file_list_delete)
            return count_records

    def insert_toggle_resolution(self, files):

        for f in files:

            file_id = ObjectId(os.path.splitext(f)[0])

            # check if it already exists

            check_record = cl_toggle_resolution.find_one(
                {
                    'campaign_id': self.get_campaign().get_id(),
                    'id': file_id,
                }
            )

            if not check_record:
                cl_toggle_resolution.insert(
                    {
                        'campaign_id': self.get_campaign().get_id(),
                        'id': file_id,
                        'state': 'publish',
                        'gender': self.get_campaign().get_gender()
                    }
                )

    def toggle(self, filename, field):

        field_value_1 = field_value_2 = ''

        if field == 'state':
            field_value_1 = 'publish'
            field_value_2 = 'removed'
        elif field == 'gender':
            field_value_1 = 'male'
            field_value_2 = 'female'

        file_id = ObjectId(os.path.splitext(filename)[0])

        pipe = {
            'id': file_id,
            'campaign_id': self.get_campaign().get_id()
        }

        record = cl_toggle_resolution.find_one(pipe)

        if record:
            previous_field = record.get(field)

            record_id = ObjectId(record['_id'])

            new_field = field_value_1 if previous_field == field_value_2 else field_value_2

            cl_toggle_resolution.update_one(
                {'_id': record_id}, {'$set': {field: new_field}}, upsert=False)

    def delete_records(self, delete_list):

        campaign_record = self.get_campaign()

        pipe = {
            'campaign_id': campaign_record.get_id()
        }

        # delete all records from campaign of the toggle resolution collection

        cl_toggle_resolution.delete_many(pipe)

        # delete from disagreed too

        count_disagreed_files = campaign_record.delete_disagreed_files(images=delete_list)

        return count_disagreed_files

    def delete_records_gender(self):

        campaign_record = self.get_campaign()

        pipe = {
            'campaign_id': campaign_record.get_id()
        }

        # delete all records from campaign of the toggle resolution gender collection

        count_gender_inaccurate = cl_toggle_resolution_gender.delete_many(pipe)

        # delete from disagreed too

        return count_gender_inaccurate.deleted_count

    def get_disagreed_data(self):

        campaign_id = self.get_campaign().get_id()

        cursor = cl_toggle_resolution.find(
            {
                'campaign_id': campaign_id
            }
        )

        list_disagreed_data = []

        for rec in cursor:
            list_disagreed_data.append(
                (str(rec.get('id')) + config.IMAGE_EXTENSION, rec.get('state'), rec.get('gender'))
            )

        return list_disagreed_data

    def toggle_each(self, filename, state_publish):

        file_id = ObjectId(os.path.splitext(filename)[0])

        pipe = {
            'id': file_id,
            'campaign_id': self.get_campaign().get_id()
        }

        record = cl_toggle_resolution.find_one(pipe)

        if record is None:
            pipe.update({'state': 'removed'})
            cl_toggle_resolution.insert_one(pipe)
        else:

            state = 'publish' if state_publish == 'yes' else 'removed'

            record_id = ObjectId(record['_id'])

            cl_toggle_resolution.update_one(
                {'_id': record_id}, {'$set': {'state': state}}, upsert=False)

    def filter_resolved(self, dict_votes_disagreed):

        resolved_records = self.get_client_resolve().find(
            {
                'campaign_id': self.get_campaign().get_id(),
            }
        )

        for rec in resolved_records:

            key = rec.get('id')

            if key in dict_votes_disagreed.keys():
                # remove from dictionary

                dict_votes_disagreed.pop(key)

        return dict_votes_disagreed

    def count_positive_resolved(self, gender):

        """

        count_records = self.get_client_resolve().count(
            {
                'campaign_id': self.get_campaign().get_id(),
                'state': 'publish',
                'gender': gender
            }
        )

        """

        count_records = 0

        resolved = self.get_client_resolve().find(
            {
                'campaign_id': self.get_campaign().get_id(),
                'state': 'publish',
                'gender': gender
            }
        )

        deleted = model_deleted.get_deleted()

        for r in resolved:
            if r.get('id') not in deleted:
                count_records += 1

        return count_records

    def get_positive_resolved(self, gender):

        cursor = self.get_client_resolve().find(
            {
                'campaign_id': self.get_campaign().get_id(),
                'state': 'publish',
                'gender': gender
            }
        )

        list_files = []

        deleted = model_deleted.get_deleted()

        for c in cursor:

            if c.get('id') not in deleted:

                list_files.append(c.get('id'))

        return list_files

    def submit_resolution_gender(self):

        # get all records corresponding to campaign on the toggle resolution gender table

        cursor = cl_toggle_resolution_gender.find(
            {
                'campaign_id': self.get_campaign().get_id(),
            }
        )

        for rec in cursor:
            file_id = rec.get('id')
            gender = rec.get('gender')

            # change the gender

            self.campaign.change_inaccurate_gender(file_id, gender)

        # delete records

        count_records = self.delete_records_gender()
        return count_records

    def insert_toggle_resolution_gender(self, list_files):

        for f in list_files:

            file_id = ObjectId(os.path.splitext(f)[0])

            # check if it already exists

            check_record = cl_toggle_resolution_gender.find_one(
                {
                    'campaign_id': self.get_campaign().get_id(),
                    'id': file_id,
                }
            )

            if not check_record:
                cl_toggle_resolution_gender.insert(
                    {
                        'campaign_id': self.get_campaign().get_id(),
                        'id': file_id,
                        'gender': self.get_campaign().get_gender()
                    }
                )

    def get_gender_inaccurate_data(self):

        campaign_id = self.get_campaign().get_id()

        cursor = cl_toggle_resolution_gender.find(
            {
                'campaign_id': campaign_id
            }
        )

        list_gender_inaccurate = []

        for rec in cursor:
            list_gender_inaccurate.append(
                (str(rec.get('id')) + config.IMAGE_EXTENSION, rec.get('gender'))
            )

        return list_gender_inaccurate

    def toggle_gender_inaccurate(self, filename, gender):

        self._toggle_gender(filename, gender, cl_toggle_resolution_gender)

    def toggle_gender(self, filename, gender):

        self._toggle_gender(filename, gender, cl_toggle_resolution)

    def _toggle_gender(self, filename, gender, collection):
        """
Remake of toggle gender function
        :param filename:
        :param gender:
        :param collection:
        """

        file_id = ObjectId(os.path.splitext(filename)[0])

        pipe = {
            'id': file_id,
            'campaign_id': self.get_campaign().get_id()
        }

        record = collection.find_one(pipe)

        if record:
            record_id = ObjectId(record['_id'])

            new_gender = 'male' if str(gender) == 'female' else 'female'

            collection.update_one(
                {'_id': record_id}, {'$set': {'gender': new_gender}}, upsert=False)


def change_resolved(file_name, new_gender):
    cl_resolve.update_one(
        {'id': ObjectId(file_name)}, {'$set': {'gender': new_gender}}, upsert=False)