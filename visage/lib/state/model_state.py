import os
from bson import ObjectId
from db_config import client_db

cl_toggle_state = client_db["YAHOO_REST"]["toggle_state"]


class State:
    def __init__(self, user, campaign):
        self.user = user
        self.campaign = campaign

    def get_campaign(self):
        return self.campaign

    def get_user(self):
        return self.user

    def set_user(self, user):
        self.user = user

    def get_toggle_state(self, id_record):

        toggle_record = cl_toggle_state.find_one(
                    {
                        'id': id_record,
                        'user': self.get_user(),
                        'campaign_id': self.get_campaign().get_id()
                    }
                )

        return toggle_record

    def get_toggle_files(self):
        files = []

        cursor = self.get_toggle(None)  # todo ? is this correct

        for rec in cursor:
            files.append(rec['filename'])

        return files

    def toggle_state_publish(self, filename):
        self.toggle_state(filename)

    def _get_state(self, filename):
        pipe = {
            'filename': filename,
            'id': ObjectId(os.path.splitext(filename)[0]),
            'user': self.get_user(),
            'campaign_id': self.get_campaign().get_id()
        }

        if cl_toggle_state.find_one(pipe).count() > 0:
            record = cl_toggle_state.find_one(pipe)

            state = record['state']

            return state

    def get_toggle(self, state):

        cursor = cl_toggle_state.find({
            'user': self.get_user(),
            'campaign_id': self.get_campaign().get_id(),
            'state': state
        })

        return cursor

    def get_removed_files(self):

        state = 'removed'

        files = []

        cursor = self.get_toggle(state)

        for rec in cursor:
            files.append(rec['filename'])

        return files

    def toggle_state(self, filename):

        file_id = ObjectId(os.path.splitext(filename)[0])

        pipe = {
            'filename': filename,
            'id': file_id,
            'user': self.get_user(),
            'campaign_id': self.get_campaign().get_id()
        }

        record = cl_toggle_state.find_one(pipe)

        if record is None:
            pipe.update({'state': 'removed'})
            cl_toggle_state.insert_one(pipe)
        else:

            previous_state = record.get('state')

            record_id = ObjectId(record['_id'])

            # check if input is multiple face then state must be removed

            type_face = record.get('type_faces')

            new_state = 'publish' if previous_state == 'removed' else 'removed'

            campaign_type_faces = self.get_campaign().get_type_faces()

            if campaign_type_faces == 'single':

                if type_face == 'multiple' and new_state == 'publish':
                    print('making a multiple face valid is not possible')
                else:
                    cl_toggle_state.update_one(
                        {'_id': record_id}, {'$set': {'state': new_state}}, upsert=False)
            elif campaign_type_faces == 'multiple':
                # TODO implement
                pass

    def record_type_faces(self, filename, type_faces):

        file_id = ObjectId(os.path.splitext(filename)[0])

        pipe = {
            'filename': filename,
            'id': file_id,
            'user': self.get_user(),
            'campaign_id': self.get_campaign().get_id()
        }

        record = cl_toggle_state.find_one(pipe)

        if record is not None:
            record_id = ObjectId(record['_id'])
            cl_toggle_state.update_one({'_id': record_id}, {'$set': {'type_faces': type_faces}}, upsert=False)

    def record_gender(self, filename, gender):

        file_id = ObjectId(os.path.splitext(filename)[0])

        pipe = {
            'filename': filename,
            'id': file_id,
            'user': self.get_user(),
            'campaign_id': self.get_campaign().get_id()
        }

        record = cl_toggle_state.find_one(pipe)

        if record is not None:
            record_id = ObjectId(record['_id'])
            cl_toggle_state.update_one(
                {
                    '_id': record_id
                },
                {
                    '$set': {'gender': gender, 'state': 'publish'}
                }, upsert=False)

    def record_age(self, filename, age):

        file_id = ObjectId(os.path.splitext(filename)[0])

        pipe = {
            'filename': filename,
            'id': file_id,
            'user': self.get_user(),
            'campaign_id': self.get_campaign().get_id()
        }

        record = cl_toggle_state.find_one(pipe)

        if record is not None:
            record_id = ObjectId(record['_id'])
            cl_toggle_state.update_one({'_id': record_id}, {'$set': {'manual_age': int(age)}}, upsert=False)

    def get_state(self):
        cursor = cl_toggle_state.find({
            'user': self.get_user(),
            'campaign_id': self.get_campaign().get_id(),
        })

        return cursor

    def delete_records(self):
        pipe = {
            'user': self.get_user(),
            'campaign_id': self.get_campaign().get_id()
        }

        cl_toggle_state.delete_many(pipe)

    def toggle_state_all(self, filename, state_publish):

        file_id = ObjectId(os.path.splitext(filename)[0])

        pipe = {
            'filename': filename,
            'id': file_id,
            'user': self.get_user(),
            'campaign_id': self.get_campaign().get_id()
        }

        record = cl_toggle_state.find_one(pipe)

        if record is None:
            pipe.update({'state': 'removed'})
            cl_toggle_state.insert_one(pipe)
        else:

            state = 'publish' if state_publish == 'yes' else 'removed'

            record_id = ObjectId(record['_id'])

            # check in db if campaign value is the same as type_face

            if record.get('type_faces') == self.get_campaign().get_type_faces() or state == 'removed':

                cl_toggle_state.update_one(
                    {'_id': record_id}, {'$set': {'state': state}}, upsert=False)
            else:
                print('type of face should match with campaign is incompatible to allow the state change')


def insert_single_toggle_state(pipe):
    cl_toggle_state.insert_one(pipe)


def get_state_single(pipe):
    record = cl_toggle_state.find_one(pipe)

    return record
