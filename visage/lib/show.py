# -*- coding: utf-8 -*-
"""
    showoff.lib.show
    ~~~~~~~~~~~~~~~~

    :copyright: (c) 2010-2014 by Jochem Kossen.
    :license: BSD, see LICENSE.txt for more details.
"""
import os
import re
from bson import ObjectId
import config
from visage.lib.database_manager import number_items, get_data, get_random_image
from visage.lib.exceptions import UnknownFileError
from visage.lib.state.model_state import State
from visage.lib.vote.model_vote import Vote


class Show(object):

    def __init__(self, user, campaign):
        self.user = user
        self.campaign = campaign
        self.album_dir = os.path.join(config.ALBUMS_DIR, str(campaign.get_age()))
        self.show_dir = os.path.join(config.SHOWS_DIR, str(campaign.get_age()))

        self.data = {
            'files': [],
            'toggle': []
        }

        self.method = None
        self.max_progress = None

    def set_user(self, user):
        self.user = user

    def get_user(self):
        return str(self.user)

    def __repr__(self):
        return '<Show for album %s>' % self.get_campaign().get_age()

    def __iter__(self):
        return iter(self.data['files'])

    @property
    def nr_of_items_db(self):
        """Number of items in database show
        :return: number of items
        """
        # filter gradually with
        vote = Vote(user=self.get_user(), campaign=self.get_campaign())
        voted_user = vote.filter_voted()
        voted_all = vote.filter_voted_all()
        voted_inverse_gender = vote.filter_voted_inverse_gender()
        # add 2 negative response filter
        voted_two_negatives = vote.get_negative_votes(2)
        voted = voted_user + voted_all + voted_inverse_gender + voted_two_negatives

        return number_items(voted_items=voted, campaign=self.get_campaign(), method=self.get_method())

    @property
    def nr_of_items(self):
        """Number of items in show"""
        return len(self.data['files'])

    @property
    def is_enabled(self):
        """Check if show is enabled"""
        return self.nr_of_items_db > 0

    def _ensure_file_exists(self, filename):
        """Check if album contains image

           Args:
             filename (string): filename of the image

           Raises:
             UnknownFileError: if file does not exist in album
        """
        if filename not in os.listdir(self.album_dir):
            raise UnknownFileError

    def remove_user(self, username):
        """Remove user from show

           Args: username (string): username

           Returns:
             self
        """
        self.data['users'].pop(username)
        return self

    def get_data(self):
        return self.data

    def set_data(self, data):
        self.data = data

    def set_campaign(self, campaign):
        self.campaign = ObjectId(campaign)

    def get_campaign(self):
        return self.campaign

    def get_method(self):
        return self.method

    def set_method(self, method):
        self.method = method

    def load(self, number_images):
        """Load the show data

           Returns:
             self
        """

        # if first time, set to gender prediction (this will be done by database)

        vote = Vote(user=self.get_user(), campaign=self.get_campaign())
        voted_user = vote.filter_voted()    # voted by user
        voted_all = vote.filter_voted_all()  # voted by all
        voted_inverse_gender = vote.filter_voted_inverse_gender()  # set to other gender

        # add 2 negative vote filter

        voted_two_negatives = vote.get_negative_votes(2)

        voted = voted_user + voted_all + voted_inverse_gender + voted_two_negatives

        if number_images != 1:
            dict_data = get_data(
                voted_items=voted,
                user=self.get_user(),
                number_images=number_images,
                campaign=self.get_campaign(),
                method=self.get_method()
            )
        else:

            dict_data = get_random_image(
                voted_items=voted,
                user=self.get_user(),
                campaign=self.get_campaign(),
                method=self.get_method())

        files_list = []

        if dict_data is not None:
            for key in dict_data:
                files_list.append(key)

        state = State(user=self.get_user(), campaign=self.get_campaign())

        json_files = {
            'files': files_list,
            'dict_data': dict_data,
            'removed':  state.get_removed_files(),
        }

        self.data.update(json_files)

        return self

    def toggle_image(self, filename):
        """Toggle if image is part of show

           Returns:
             self
        """
        self._ensure_file_exists(filename)

        if filename in self.data['files']:
            self.data['files'].remove(filename)
        else:
            self.data['files'].append(filename)

        return self

    def add_image(self, filename):
        """Add image from album to show

           Returns:
             self
        """
        self._ensure_file_exists(filename)

        if filename not in self.data['files']:
            self.data['files'].append(filename)

        return self

    def remove_image(self, filename):
        """Remove image from show (not from album)

           Returns:
             self
        """
        self._ensure_file_exists(filename)

        if filename in self.data['files']:
            self.data['files'].remove(filename)

        return self

    def sort_by_filename(self):
        """Sort the show contents by date retrieved from EXIF data

           Returns:
             self
        """
        self.data['files'].sort()

        return self

    def add_all_images(self):
        """Add all images in album to the show

           Returns:
             self
        """
        files = os.listdir(self.album_dir)

        # only list .jpg, .png, .gif, and .bmp files
        ext = re.compile(".(jpg|png|gif|bmp)$", re.IGNORECASE)
        files = [f for f in files if ext.search(f)]
        files.sort()

        for filename in files:
            self.add_image(filename)

        return self
