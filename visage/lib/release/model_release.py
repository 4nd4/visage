import datetime
import os

from db_config import client_db

cl_release = client_db["YAHOO_REST"]["release"]


class Release:
    def __init__(self, version, file_path, description, data):
        self.version = version
        self.file_path = file_path
        self.description = description
        self.data = data

    def get_version(self):
        return self.version

    def get_file(self):
        return self.file_path

    def get_description(self):
        return self.description

    def get_data(self):
        return self.data

    def get_size(self):

        if os.path.exists(self.get_file()):
            return os.path.getsize(self.get_file())

    def get_hash(self):

        import hashlib

        block_size = 65536

        hasher = hashlib.md5()

        if not os.path.exists(self.get_file()):

            print('file not found', self.get_file())

            return None

        with open(self.get_file(), 'rb') as afile:
            buf = afile.read(block_size)
            while len(buf) > 0:
                hasher.update(buf)
                buf = afile.read(block_size)
        return hasher.hexdigest()

    def insert(self):

        check_release = cl_release.find_one(
            {
                'version': self.get_version()
            }
        )

        if check_release:
            print('release version already existent')
            return False

        md5 = self.get_hash()

        check_hash = cl_release.find_one(
            {
                'MD5': md5
            }
        )

        if check_hash:
            print('file with that hash already exists')
            return False

        insert_pipe = {
                'version': self.get_version(),
                'date': datetime.datetime.today(),
                'description': self.get_description(),
                'data': self.get_data()
            }

        if os.path.exists(self.get_file()):
            insert_pipe.update(
                {
                    'MD5': md5,
                    'size': self.get_size()
                }
            )

        cl_release.insert(
            insert_pipe
        )

    def delete(self):
        pass

    def update(self):
        pass
