from bson import ObjectId

import config
from db_config import client_db
from visage.lib.campaign import model_campaign
from visage.lib.campaign.model_campaign import Campaign, get_campaign_transition
from visage.lib.deleted import model_deleted
from visage.lib.resolve.model_resolve import Resolve
from visage.lib.vote.model_vote import Vote


class Statistics:
    def __init__(self, user):
        self.database = 'YAHOO_REST'
        self.collection_data = 'data'
        self.collection_image_creation = 'image_created'
        self.collection_vote = 'vote'
        self.collection_user = 'user'
        self.client_data = client_db[self.get_database()][self.get_collection_data()]
        self.client_image_creation = client_db[self.get_database()][self.get_collection_image_creation()]
        self.client_vote = client_db[self.get_database()][self.get_collection_vote()]
        self.client_user = client_db['RESEARCH_TOOLS'][self.get_collection_user()]
        self.age_min = 1
        self.age_max = 100
        self.user = user
        self.votes = None

    def get_user(self):
        return self.user

    def get_database(self):
        return self.database

    def get_collection_data(self):
        return self.collection_data

    def get_collection_image_creation(self):
        return self.collection_image_creation

    def get_collection_vote(self):
        return self.collection_vote

    def get_collection_user(self):
        return self.collection_user

    def get_client_data(self):
        return self.client_data

    def get_client_image_creation(self):
        return self.client_image_creation

    def get_client_vote(self):
        return self.client_vote

    def get_client_user(self):
        return self.client_user

    def get_age_min(self):
        return self.age_min

    def get_age_max(self):
        return self.age_max

    def get_photos_metadata(self):

        return self.get_client_data().find().count()

    def get_photos_download(self):

        return self.get_client_image_creation().find().count()

    def get_photos_azure(self):
        return self.get_client_image_creation().find(
            {
                'azure_service': {'$exists': True}
            }
        ).count()

    def get_metadata_per_age(self):

        cursor = self.get_client_data().aggregate(
            [
                {
                    '$group':
                        {
                            '_id': '$age',
                            'count': {'$sum': 1}
                        }
                },
                {
                    '$sort':
                        {
                            '_id': 1
                        }
                }

            ]
        )

        dict_result = {}

        for c in cursor:
            dict_result[c.get('_id')] = {'count': c.get('count')}

        return dict_result

    def get_downloads_per_age(self):
        cursor = self.get_client_image_creation().aggregate(
            [
                {
                    '$group':
                        {
                            '_id': '$age',
                            'count': {'$sum': 1}
                        }
                },
                {
                    '$sort':
                        {
                            '_id': 1
                        }
                }

            ]
        )

        dict_result = {}

        for c in cursor:
            dict_result[c.get('_id')] = {'count': c.get('count')}

        return dict_result

    def get_processed_azure_per_age(self):
        cursor = self.get_client_image_creation().aggregate(
            [
                {
                    '$match':
                        {
                            'azure_service': {'$exists': True}
                        }
                },
                {
                    '$group':
                        {
                            '_id': '$age',
                            'count': {'$sum': 1}
                        }
                },
                {
                    '$sort':
                        {
                            '_id': 1
                        }
                }

            ]
        )

        dict_result = {}

        for c in cursor:
            dict_result[c.get('_id')] = {
                'count': c.get('count')
            }

        return dict_result

    def get_processed_azure_per_age_single_faces(self):
        cursor = self.get_client_image_creation().aggregate(
            [
                {
                    '$match':
                        {
                            'azure_service': {'$size': 1}
                        }
                },
                {
                    '$group':
                        {
                            '_id': '$age',
                            'count': {'$sum': 1}
                        }
                },
                {
                    '$sort':
                        {
                            '_id': 1
                        }
                }

            ]
        )

        dict_result = {}

        for c in cursor:
            dict_result[c.get('_id')] = {
                'count': c.get('count')
            }

        return dict_result

    def get_votes_per_age(self, number_votes, condition):

        # get 2 votes that aren't both negative

        pipe = [
            {
                '$group': {
                    '_id':
                        {
                            'image_id': '$id',
                            'campaign_id': '$campaign_id'
                        },
                    'users': {'$push': {'user': '$user'}},
                    'votes': {'$push': {'state': '$state'}},
                    'count': {'$sum': 1}
                }
            },
            {
                '$match': {'count': {condition: number_votes}}
            }
        ]

        cursor = self.get_client_vote().aggregate(
            pipe, allowDiskUse=True
        )

        dict_result = {}

        for c in cursor:

            # don't consider 2 negative votes

            campaign_id = str(c.get('_id').get('campaign_id'))

            campaign = Campaign(campaign_id=campaign_id)

            image_id = ObjectId(c.get('_id').get('image_id'))

            votes = c.get('votes')

            count_votes = int(c.get('count'))

            if count_votes == 2 and (votes[0].get('state') == 'no' and votes[1].get('state') == 'no'):
                pass
            else:
                    dict_result[str(image_id), campaign_id] = {
                        'age': campaign.get_age(),
                        'users': c.get('users'),
                        'count': count_votes
                    }

        return dict_result

    def get_votes_disagreed(self, number_votes, age, gender):
        pipe = [
            {
                '$match':
                    {
                        '$and': [{'age_campaign': age}, {'gender_campaign': gender}]
                    }
            },
            {
                '$group':
                    {
                        '_id': '$id',
                        'votes': {'$push': {'state': '$state'}},
                        'count': {'$sum': 1}
                    }
            },
            {
                '$match':
                    {
                        # 'count': {'$eq': number_votes},
                        '$and': [{'count': {'$eq': number_votes}}, {'votes.state': 'yes'}, {'votes.state': 'no'}]
                    }

            }
        ]

        cursor = self.get_client_vote().aggregate(
            pipe
        )

        dict_result = {}

        for c in cursor:

            votes = c.get('votes')

            # get votes that have 2 positive votes and 1 negative

            list_votes = [v.get('state') for v in votes]

            if list_votes.count('yes') == 2:
                dict_result[c.get('_id')] = {
                    'votes': c.get('votes'),
                    'count': c.get('count')
                }

        return dict_result

    def get_votes_gender_inconsistent(self, number_votes, campaign):

        age = campaign.get_age()
        gender = campaign.get_gender()

        pipe = [
            {
                '$match':
                    {
                        '$and': [{'age_campaign': age}, {'gender_campaign': gender}, {'state': 'yes'}]
                    }
            },
            {
                '$group':
                    {
                        '_id': '$id',
                        'votes': {'$push': {'gender': '$gender'}},
                        'count': {'$sum': 1}
                    }
            },
            {
                '$match':
                    {
                        '$and': [{'count': {'$eq': number_votes}}, {'votes.gender': 'male'}, {'votes.gender': 'female'}]
                    }

            }
        ]

        cursor = self.get_client_vote().aggregate(
            pipe
        )

        list_result = []

        for c in cursor:
            list_result.append(c.get('_id'))

        return list_result

    def get_positive_votes(self, number_votes, age, gender, user_check):

        c = Campaign(age=age, gender=gender, type_faces='single')

        v = Vote(self.get_user(), c)

        cursor = v.get_cursor_positive_votes(number_votes=number_votes, age=age, gender=gender, user_check=user_check)

        dict_result = {}

        if number_votes < config.NUMBER_VOTES:

            r = Resolve(c, self.get_user())

            for x in cursor:

                record = r.get_client_resolve().find_one(
                    {
                        'id': ObjectId(x.get('_id')),
                        'gender': c.get_inverse_gender_campaign().get_gender()
                    }
                )

                if record is None:
                    dict_result[x.get('_id')] = {
                        'count': x.get('count')
                    }

        elif number_votes == config.NUMBER_VOTES:

            for x in cursor:
                dict_result[x.get('_id')] = {
                    'count': x.get('count')
                }

            # get all items that are in transition

            campaign_transition = get_campaign_transition(age=age, gender=gender, number_votes=number_votes)

            for tran in campaign_transition:

                # key_tuple = tran.get('_id'), tran.get('campaign_new')

                key_transition_image = tran.get('_id')

                if key_transition_image not in dict_result.keys():
                    dict_result[key_transition_image] = {
                        # 'age': tran.get('age'),
                        # 'gender': tran.get('gender'),
                        'count': tran.get('count')
                    }

        # get deleted values

        deleted = model_deleted.get_deleted()

        for d in deleted:
            if d in dict_result:
                del dict_result[d]

        return dict_result

    def get_table(self):

        # get general table

        dict_table = {}
        dict_sum = {}

        dict_table_general, dict_sum_general = self.get_table_general()
        dict_table_positives, dict_sum_positives = self.get_table_positives(config.NUMBER_VOTES, False)
        dict_table_missing, dict_sum_missing = self.get_table_missing()
        dict_table_disagreed, dict_sum_disagreed = self.get_table_disagreed()

        for i in range(self.get_age_min(), self.get_age_max() + 1):
            dict_table[i] = {}

            dict_table[i].update(dict_table_general[i])
            dict_table[i].update(dict_table_positives[i])
            dict_table[i].update(dict_table_missing[i])
            dict_table[i].update(dict_table_disagreed[i])

        dict_sum.update(dict_sum_general)
        dict_sum.update(dict_sum_positives)
        dict_sum.update(dict_sum_missing)
        dict_sum.update(dict_sum_disagreed)

        return dict_table, dict_sum

    def get_table_general(self):

        dict_table = {}

        dict_metadata = self.get_metadata_per_age()
        dict_downloads = self.get_downloads_per_age()
        dict_azure = self.get_processed_azure_per_age()
        dict_azure_single_faces = self.get_processed_azure_per_age_single_faces()

        sum_metadata = 0
        sum_downloads = 0
        sum_azure = 0
        sum_azure_single_faces = 0

        for i in range(self.get_age_min(), self.get_age_max() + 1):
            metadata_value = dict_metadata.get(i).get('count') if dict_metadata.get(i) is not None else 0
            downloads_value = dict_downloads.get(i).get('count') if dict_downloads.get(i) is not None else 0
            azure_value = dict_azure.get(i).get('count') if dict_azure.get(i) is not None else 0
            azure_value_single_faces = dict_azure_single_faces.get(i).get('count') \
                if dict_azure_single_faces.get(i) is not None else 0

            dict_table[i] = {
                'metadata': metadata_value,
                'downloads': downloads_value,
                'azure': azure_value,
                'azure_single_faces': azure_value_single_faces,
            }

            sum_metadata += metadata_value
            sum_downloads += downloads_value
            sum_azure += azure_value
            sum_azure_single_faces += azure_value_single_faces

        dict_sum = {
            'metadata': sum_metadata,
            'downloads': sum_downloads,
            'azure': sum_azure,
            'azure_single_faces': sum_azure_single_faces
        }

        # get sum of values

        return dict_table, dict_sum

    def get_table_missing(self):

        dict_table = {}

        dict_votes_missing = self.get_votes_per_age(config.NUMBER_VOTES, '$lt')  # todo optimize

        sum_votes_missing = 0

        for i in range(self.get_age_min(), self.get_age_max() + 1):
            votes_missing_value = get_value(dict_votes_missing, i)

            dict_table[i] = {
                'votes_missing': votes_missing_value,
            }

            sum_votes_missing += votes_missing_value

        dict_sum = {
            'votes_missing': sum_votes_missing,
        }

        # get sum of values

        return dict_table, dict_sum

    def get_table_positives(self, votes, user_check):

        dict_table = {}

        sum_votes = 0
        sum_votes_male = 0
        sum_votes_female = 0
        sum_votes_male_resolved = 0
        sum_votes_female_resolved = 0
        count_resolved_male = 0
        count_resolved_female = 0

        votes_description = get_votes_description(votes)

        for i in range(self.get_age_min(), self.get_age_max() + 1):
            dict_votes_positive_male = self.get_positive_votes(int(votes), i, 'male', user_check)
            dict_votes_positive_female = self.get_positive_votes(int(votes), i, 'female', user_check)

            # add positive resolved images either male or female

            if int(votes) == config.NUMBER_VOTES:

                campaign_male = model_campaign.Campaign(age=i, gender='male', type_faces='single')
                campaign_female = model_campaign.Campaign(age=i, gender='female', type_faces='single')

                campaign_male.set_id()
                campaign_female.set_id()

                resolve_male = Resolve(campaign=campaign_male, user=self.get_user())
                resolve_female = Resolve(campaign=campaign_female, user=self.get_user())

                resolved_positive_male_votes_male = resolve_male.count_positive_resolved('male')
                resolved_positive_female_votes_male = resolve_female.count_positive_resolved('male')

                resolved_positive_female_votes_female = resolve_female.count_positive_resolved('female')
                resolved_positive_male_votes_female = resolve_male.count_positive_resolved('female')

                count_resolved_male = resolved_positive_male_votes_male + resolved_positive_female_votes_male
                count_resolved_female = resolved_positive_female_votes_female + resolved_positive_male_votes_female

                # add images from resolved

                images_positive_male = list(
                    dict_votes_positive_male.keys() +
                    resolve_male.get_positive_resolved('male') +
                    resolve_female.get_positive_resolved('male')
                )
                images_positive_female = list(
                    dict_votes_positive_female.keys() +
                    resolve_female.get_positive_resolved('female') +
                    resolve_male.get_positive_resolved('female')
                )

                # insert into campaign collection, delete the previous list and update with this one

                campaign_male.insert_positive_images(images=images_positive_male)
                campaign_female.insert_positive_images(images=images_positive_female)

            count_votes_positive_male = len(dict_votes_positive_male) + count_resolved_male
            count_votes_positive_female = len(dict_votes_positive_female) + count_resolved_female
            count_votes_positive = count_votes_positive_male + count_votes_positive_female

            dict_table[i] = {

                votes_description: count_votes_positive,
                'votes_value_positive_male': count_votes_positive_male,
                'votes_value_positive_female': count_votes_positive_female,
                'votes_value_positive_male_resolved': count_resolved_male,
                'votes_value_positive_female_resolved': count_resolved_female
            }

            sum_votes += count_votes_positive
            sum_votes_male += count_votes_positive_male
            sum_votes_female += count_votes_positive_female
            sum_votes_male_resolved += count_resolved_male
            sum_votes_female_resolved += count_resolved_female

        dict_sum = {
            votes_description: sum_votes,
            'votes_value_positive_male': sum_votes_male,
            'votes_value_positive_female': sum_votes_female,
            'votes_value_positive_male_resolved': sum_votes_male_resolved,
            'votes_value_positive_female_resolved': sum_votes_male_resolved
        }

        # get sum of values

        return dict_table, dict_sum

    def get_table_disagreed(self):

        dict_table = {}

        sum_votes_disagreed = 0
        sum_votes_disagreed_male = 0
        sum_votes_disagreed_female = 0

        for i in range(self.get_age_min(), self.get_age_max() + 1):
            dict_votes_disagreed_male = self.get_votes_disagreed(config.NUMBER_VOTES, i, 'male')
            dict_votes_disagreed_female = self.get_votes_disagreed(config.NUMBER_VOTES, i, 'female')

            # filter votes that have been resolved

            campaign_male = model_campaign.Campaign(age=i, gender='male', type_faces='single')
            campaign_female = model_campaign.Campaign(age=i, gender='female', type_faces='single')

            campaign_male.set_id()
            campaign_female.set_id()

            resolve_male = Resolve(campaign=campaign_male, user=self.get_user())
            resolve_female = Resolve(campaign=campaign_female, user=self.get_user())

            filtered_votes_disagreed_male = resolve_male.filter_resolved(dict_votes_disagreed_male)
            filtered_votes_disagreed_female = resolve_female.filter_resolved(dict_votes_disagreed_female)

            count_votes_disagreed_male = len(filtered_votes_disagreed_male)
            count_votes_disagreed_female = len(filtered_votes_disagreed_female)

            count_votes_disagreed = count_votes_disagreed_male + count_votes_disagreed_female

            # insert disagreed

            # insert into campaign collection, delete the previous list and update with this one

            male_image_keys = list(dict_votes_disagreed_male.keys())
            female_image_keys = list(dict_votes_disagreed_female.keys())

            campaign_male.insert_disagreed_images(images=male_image_keys)
            campaign_female.insert_disagreed_images(images=female_image_keys)

            dict_table[i] = {
                'votes_disagreed': count_votes_disagreed,
                'votes_disagreed_male': count_votes_disagreed_male,
                'votes_disagreed_female': count_votes_disagreed_female,
            }

            sum_votes_disagreed += count_votes_disagreed
            sum_votes_disagreed_male += count_votes_disagreed_male
            sum_votes_disagreed_female += count_votes_disagreed_female

        dict_sum = {
            'votes_disagreed': sum_votes_disagreed,
            'votes_disagreed_male': sum_votes_disagreed_male,
            'votes_disagreed_female': sum_votes_disagreed_female
        }

        # get sum of values

        return dict_table, dict_sum

    def get_table_gender_inaccurate(self):
        dict_table = {}

        sum_votes_gender_inaccurate = 0
        sum_votes_gender_inaccurate_male = 0
        sum_votes_gender_inaccurate_female = 0

        for i in range(self.get_age_min(), self.get_age_max() + 1):
            campaign_male = Campaign(age=i, gender='male')
            campaign_male.set_id()
            campaign_female = Campaign(age=i, gender='female')
            campaign_female.set_id()

            dict_votes_gender_inaccurate_male = self.get_votes_gender_inconsistent(
                config.NUMBER_VOTES,
                campaign_male
            )

            dict_votes_gender_inaccurate_female = self.get_votes_gender_inconsistent(
                config.NUMBER_VOTES,
                campaign_female
            )

            # filter votes that have been resolved

            count_votes_gender_inaccurate_male = len(dict_votes_gender_inaccurate_male)
            count_votes_gender_inaccurate_female = len(dict_votes_gender_inaccurate_female)

            count_votes_gender_inaccurate = count_votes_gender_inaccurate_male + count_votes_gender_inaccurate_female

            dict_table[i] = {
                'votes_gender_inaccurate': count_votes_gender_inaccurate,
                'votes_gender_inaccurate_male': count_votes_gender_inaccurate_male,
                'votes_gender_inaccurate_female': count_votes_gender_inaccurate_female
            }

            sum_votes_gender_inaccurate += count_votes_gender_inaccurate
            sum_votes_gender_inaccurate_male += count_votes_gender_inaccurate_male
            sum_votes_gender_inaccurate_female += count_votes_gender_inaccurate_female

        dict_sum = {
            'votes_gender_inaccurate': sum_votes_gender_inaccurate,
            'votes_gender_inaccurate_male': sum_votes_gender_inaccurate_male,
            'votes_gender_inaccurate_female': sum_votes_gender_inaccurate_female,
        }

        # get sum of values

        return dict_table, dict_sum

    def get_sum_user(self, user):

        cursor = self.get_client_vote().aggregate(
            [
                {
                    '$match':
                        {
                            'user': user
                        }
                },
                {
                    '$group':
                        {
                            '_id': '$user',
                            'count': {'$sum': 1}
                        }
                }
            ]
        )

        for i in cursor:
            return i.get('count')
        return 0

    def get_votes_users(self):

        cursor = self.get_client_vote().aggregate(
            [
                {
                    '$group':
                        {
                            '_id':
                                {
                                    'user': '$user',
                                    'campaign_id': '$campaign_id'
                                },
                            'count': {'$sum': 1}
                        }
                }
            ]
        )

        dict_result = {}

        for c in cursor:
            dict_result[str(c.get('_id').get('user')), str(c.get('_id').get('campaign_id'))] = {'count': c.get('count')}

        return dict_result

    def get_users(self):

        list_users = []

        cursor = self.get_client_user().find()

        for c in cursor:
            list_users.append(c.get('username'))

        return list_users

    def get_campaigns(self):

        list_string_campaigns = []

        list_campaigns = self.get_client_vote().find({
        }).distinct('campaign_id')

        for c in list_campaigns:
            campaign = Campaign(campaign_id=c)

            list_string_campaigns.append(
                {
                    'id': str(campaign.get_id()),
                    'age': campaign.get_age(),
                    'gender': campaign.get_gender()
                }
            )

        return list_string_campaigns


def get_votes_description(votes):
    if str(votes).isdigit():
        i_votes = int(votes)

        if i_votes == config.NUMBER_VOTES:
            return 'votes_three'
        else:
            return 'votes_at_least_one'


def get_value(dict_result, age):
    """
    Create table with values, iterate in order 0 to 100
    :param dict_result:
    :param age:
    :return:
    """

    counter = 0

    for d in dict_result.values():
        if d.get('age'):
            if int(d.get('age')) == age:
                counter += 1

    return counter


def get_users(dict_result, age):
    """
    Create table with values, iterate in order 0 to 100
    :param dict_result:
    :param age:
    :return:
    """

    for d in dict_result.values():
        if d.get('age'):
            if int(d.get('age')) == age:
                if d.get('users'):
                    return d.get('users')


def get_value_positive_number_votes_minus_1():
    pass


def get_count_value_votes(dict_result, age, agree, gender=None):
    if agree:

        r = {k: v for k, v in dict_result.iteritems() if int(v.get('age')) == age}

    else:

        r = get_value_disagreed_votes(dict_result, age, gender)

    return len(r)


def get_value_disagreed_votes(dict_result, age, gender):
    r = {
        k: v for k, v in dict_result.iteritems()
        if int(v.get('age')) == age and v.get('gender') == gender and not check_equal(v.get('votes'))
    }

    return r


def get_value_votes_gender(dict_result, age, gender):
    r = {k: v for k, v in dict_result.iteritems() if int(v.get('age')) == age and v.get('gender') == gender}

    return r


def check_equal(lst):
    return lst[1:] == lst[:-1]
