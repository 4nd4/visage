import bcrypt
from db_config import client_db

cl_user = client_db["RESEARCH_TOOLS"]["user"]


class User:
    def __init__(self, user, password):
        self.user = user
        self.email = None
        self.password = password

    def get_user(self):
        return self.user

    def set_user(self, user):
        self.user = user

    def get_email(self):
        return self.email

    def set_email(self, email):
        self.email = email

    def get_password(self):
        return self.password

    def set_password(self, password):
        self.password = password

    def register_user(self):
        users = cl_user
        existing_user = users.find_one({'username': self.get_user()})
        existing_email = users.find_one({'email': self.get_email()})

        if existing_email is not None:
            print('That email is already assigned')
            return False
        elif existing_user is None:

            hash_pass = bcrypt.hashpw(self.get_password().encode('utf-8'), bcrypt.gensalt())
            users.insert(
                {
                    'username': self.get_user(),
                    'email': self.get_email(),
                    'password': hash_pass
                })

            return True

        print('That username already exists!')
        return False

    def authenticate_user(self):
        users = cl_user
        login_user = users.find_one({'username': self.get_user()})

        if login_user:
            if bcrypt.hashpw(
                    self.get_password().encode('utf-8'),
                    login_user['password'].encode('utf-8')) == login_user['password'].encode('utf-8'):
                return True


def get_users():
    records = cl_user.find()

    users = []

    for r in records:
        users.append(r.get('username'))

    return users
