import os
import random
import re
from bson import ObjectId
import config
import db_config
from visage.lib.state.model_state import State, insert_single_toggle_state, get_state_single

cl_discarded = db_config.client_db["YAHOO_REST"]["discarded"]
cl_images_created = db_config.client_db["YAHOO_REST"]["image_created"]
cl_image_data = db_config.client_db["YAHOO_REST"]["data"]


def get_azure_info(filename):
    object_id = ObjectId(os.path.splitext(filename)[0])

    record = cl_images_created.find_one(
        {
            'id': object_id,
        },
        {
            '_id': 0,
            str('azure_service'): 1,
            str('dlib_service'): 1
        }
    )

    return record


def get_flickr_info(filename):
    object_id = ObjectId(os.path.splitext(filename)[0])

    record = cl_image_data.find_one(
        {
            '_id': object_id
        },
        {
            '_id': 0,
        }
    )

    if record is not None:

        record_id = record.get('id')

        if record_id is not None and type(record_id) is ObjectId:

            del record['id']

        dict_flickr = {
            'flickr_db': record
        }

        return dict_flickr


def lookup(dic, keys):
    if len(keys) == 1:
        if dic:
            return dic.get(keys[0])
        else:
            return None
    else:
        return lookup(dic.get(keys[0]), keys[1:])


def censor_sensitive_information(record):

    censored_dict = dict(record)

    keys = ['flickr_db.owner.location', 'flickr_db.location']

    for k in keys:

        key_list = k.split('.')

        location_value = lookup(censored_dict, key_list)

        if location_value is not None:

            sub = censored_dict

            for i in key_list[:-1]:
                sub = sub[i]

            del sub[key_list[-1]]

    return censored_dict


def get_image_info(filename):
    object_id = ObjectId(os.path.splitext(filename)[0])

    record = cl_image_data.find_one(
        {
            '_id': object_id
        }
    )

    return record


def get_images_detail(list_subjects):
    pipe = [

        {
            '$match':
                {
                    '_id': {'$in': list_subjects}
                }
        },
        {
            '$group': {
                '_id':
                    {
                        'user_id': '$owner.nsid',
                    },
                # 'users': {'$push': {'user': '$user'}},
                'count': {'$sum': 1}
            }
        },
        {
            '$match': {'count': {'$lt': 3}}
        }
    ]

    records = cl_image_data.aggregate(pipe)

    return records


def get_face_type(subject_id, method):
    # extend method

    if method == 'dlib':

        # cnn here

        cl_image = db_config.client_db["YAHOO_REST"]["data"]
        id_field = '_id'

        cursor_data = cl_image.find_one(
            {
                id_field: ObjectId(os.path.splitext(subject_id)[0])
            }
        )

        if cursor_data:
            face_recognition = cursor_data.get('face_recognition')

            if face_recognition:
                cnn_dlib = face_recognition.get('cnn_dlib')

                if cnn_dlib:
                    number_faces = len(cnn_dlib)

                    if number_faces == 0:
                        return 'others'
                    elif number_faces == 1:
                        return 'single'
                    else:
                        return 'multiple'
        else:
            print('no face recognition available for object', subject_id)

    elif method == 'azure':

        cl_image = db_config.client_db["YAHOO_REST"]["image_created"]
        id_field = 'id'

        cursor_data = cl_image.find_one(
            {
                id_field: ObjectId(os.path.splitext(subject_id)[0])
            }
        )

        if cursor_data:
            azure_service = cursor_data.get('azure_service')

            if azure_service:
                number_faces = len(azure_service)

                if number_faces == 0:
                    return 'others'
                elif number_faces == 1:
                    return 'single'
                else:
                    return 'multiple'
        else:
            print('no face recognition available for object', subject_id)


def check_face_type(type_faces, subject_id):
    cursor_data = cl_image_data.find_one(
        {
            '_id': ObjectId(subject_id),
            'face_recognition.cnn_dlib': {'$size': 1},
            'face_recognition.hog_dlib': {'$size': 1}
        }
    )

    if not cursor_data:
        return False
    else:
        if 'face_recognition' in cursor_data:
            face_recognition = cursor_data.get('face_recognition')

            if 'cnn_dlib' in face_recognition:
                number_faces = len(face_recognition.get('cnn_dlib'))

                if (type_faces == 'single' and number_faces != 1) or (
                        type_faces == 'multiple' and number_faces <= 1) or (
                        type_faces == 'others' and number_faces != 0):
                    return False
    return True


def get_items(voted_items, campaign, method, limit_items):
    age = campaign.get_age()
    type_faces = campaign.get_type_faces()
    gender = campaign.get_gender()

    id_field = None
    cl_image = None
    methods = None
    extra = None

    pipe = {
        'age': int(age)
    }

    if method == 'dlib':

        cl_image = db_config.client_db["YAHOO_REST"]["data"]
        id_field = '_id'

        if type_faces == 'single' or type_faces == 'others':

            methods = {
                'face_recognition.cnn_dlib': {'$size': get_faces_detected(type_faces)},
                'face_recognition.hog_dlib': {'$size': get_faces_detected(type_faces)}
            }

            if type_faces == 'single' and gender in ['male', 'female']:
                regex = re.compile("^{0}".format(gender), re.IGNORECASE)

                extra = {'gender_recognition.how_old_net': regex}

        elif type_faces == 'multiple':

            nor_pipe = [
                {'face_recognition.cnn_dlib': {'$size': 0}},
                {'face_recognition.cnn_dlib': {'$size': 1}},
                {'face_recognition.hog_dlib': {'$size': 0}},
                {'face_recognition.hog_dlib': {'$size': 1}},
            ]

            methods = {
                'face_recognition': {'$exists': True},
                '$nor': nor_pipe
            }

    elif method == 'azure':

        cl_image = db_config.client_db["YAHOO_REST"]["image_created"]
        id_field = 'id'

        if type_faces == 'single' or type_faces == 'others':

            methods = {
                'azure_service': {'$size': get_faces_detected(type_faces)}
            }

            if type_faces == 'single' and gender in ['male', 'female']:
                regex = re.compile("^{0}".format(gender), re.IGNORECASE)

                extra = {'azure_service.faceAttributes.gender': regex}

        elif type_faces == 'multiple':
            nor_pipe = [
                {'azure_service': {'$size': 0}},
                {'azure_service': {'$size': 1}}
            ]

            methods = {
                '$nor': nor_pipe
            }

    pipe.update({id_field: {'$nin': voted_items}})

    if methods:
        pipe.update(methods)

    if extra:
        pipe.update(extra)

    return cl_image.find(pipe).limit(limit_items), id_field


def number_items(voted_items, campaign, method):
    results, id_field = get_items(voted_items=voted_items, campaign=campaign, method=method, limit_items=0)

    return results.count()


def get_faces_detected(type_faces):
    return 1 if type_faces == 'single' else 0


def get_random_image(voted_items, user, campaign, method):
    dict_data = {}

    records, id_field = get_items(voted_items=voted_items, campaign=campaign, method=method, limit_items=0)

    count = records.count()

    if count > 0:

        rec = records[random.randrange(count)]

        if rec:
            gender_tag = None
            gender = None

            filename = str(rec[id_field]) + str(config.IMAGE_EXTENSION)

            manual_age = get_manual_age(user=user, filename=filename, album=campaign.get_age())

            if campaign.get_type_faces() == 'single':
                gender = get_gender(user=user, filename=filename, album=campaign.get_age(), method=method)

                # insert in toggle table consider if already there then do not do anything
                # include gender

            if method == 'dlib':
                gender_tag = 'how_old_net'
            elif method == 'azure':
                gender_tag = 'azure_service'

            dict_data[filename] = {
                'gender': gender,
                'manual_age': manual_age,
                gender_tag: get_gender_prediction(filename, method)
            }

            return dict_data
        else:
            print('no more values available')


def get_data(voted_items, user, number_images, campaign, method):
    gender_tag = None

    cursor, id_field = get_items(voted_items=voted_items, campaign=campaign, method=method, limit_items=number_images)

    dict_data = {}

    if method == 'dlib':
        gender_tag = 'how_old_net'
    elif method == 'azure':
        gender_tag = 'azure_service'

    for rec in cursor:

        filename = str(rec[id_field]) + config.IMAGE_EXTENSION

        manual_age = get_manual_age(user=user, filename=filename, album=campaign.get_age())

        if campaign.get_type_faces() == 'single':

            gender = get_gender(user=user, filename=filename, album=campaign.get_age(), method=method)

            type_faces = get_type_face(user=user, filename=filename, album=campaign.get_age(), method=method)

            dict_data[filename] = {
                'gender': gender,
                'manual_age': manual_age,
                gender_tag: get_gender_prediction(filename, method)
            }

            # insert in toggle table consider if already there then do not do anything
            # include gender
        else:

            # type_faces = get_face_type(subject_id=filename, method=method)

            type_faces = str(campaign.get_type_faces())

            gender = None

            dict_data[filename] = {
                'gender': gender,
                'manual_age': manual_age,
                gender_tag: gender,

            }

        dict_data[filename].update({'type_faces': type_faces})

        id_record = ObjectId(rec[id_field])

        # check for the user

        state = State(user=user, campaign=campaign)

        toggle_record = state.get_toggle_state(id_record=id_record)

        if toggle_record is None:

            # get gender_campaign

            gender_campaign = campaign.get_gender()

            if gender_campaign == 'male' and gender in ['female', 'none']:
                toggle_state = 'removed'
            elif gender_campaign == 'female' and gender in ['male', 'none']:
                toggle_state = 'removed'
            else:
                toggle_state = 'publish'

            pipe = {
                'filename': filename,
                'id': id_record,
                'user': user,
                'album': str(campaign.get_age()),
                'state': toggle_state,
                'gender': gender,
                'manual_age': manual_age,
                'type_faces': campaign.get_type_faces(),
                'campaign_id': campaign.get_id()
            }

            insert_single_toggle_state(pipe)

    # add images to toggle table by default but do it only once

    return dict_data


def get_gender_prediction(filename, method):
    file_id = ObjectId(os.path.splitext(filename)[0])
    service = None

    if method == 'dlib':
        id_tag = '_id'
        cl_image = db_config.client_db["YAHOO_REST"]["data"]
        service_tag = 'gender_recognition'
        gender_tag = 'how_old_net'

        pipe = {
            id_tag: file_id,
            service_tag: {'$exists': True},
        }

        record = cl_image.find_one(pipe)

        if record is not None:
            service = record.get(service_tag)

        if service is not None:
            return service.get(gender_tag)

    elif method == 'azure':
        id_tag = 'id'
        cl_image = db_config.client_db["YAHOO_REST"]["image_created"]
        service_tag = 'azure_service'
        gender_tag = 'gender'

        pipe = {
            id_tag: file_id,
            service_tag: {'$exists': True},
        }

        record = cl_image.find_one(pipe)

        if record is not None:
            service = record.get(service_tag)

            if len(service) == 1:
                face_attributes = service[0].get('faceAttributes')
                return face_attributes.get(gender_tag)
            else:
                # log error
                print('warning, face not single', filename)


def get_gender_prediction_how_old(filename):
    file_id = ObjectId(os.path.splitext(filename)[0])

    pipe = {
        '_id': file_id,
        'gender_recognition': {'$exists': True}
    }

    record = cl_image_data.find_one(pipe)

    if record is not None and 'gender_recognition' in record:
        gender_recognition = record.get('gender_recognition')
        return gender_recognition.get('how_old_net')


def get_gender_prediction_azure(filename):
    service_tag = 'azure_service.faceAttributes'

    file_id = ObjectId(os.path.splitext(filename)[0])

    pipe = {
        'id': file_id,
        service_tag: {'$exists': True}
    }

    record = cl_images_created.find_one(pipe)

    if record is not None:
        azure_service = record.get(service_tag)

        return azure_service.get('age')


def get_gender(user, album, filename, method):
    """
If the user hasn't loaded the images before, he gets by default gender predictions from either azure or how-old.
Otherwise, the data will be retrieved from MongoDB
    :param album:
    :param user:
    :param filename:
    :param method:
    :return:
    """
    file_id = ObjectId(os.path.splitext(filename)[0])

    pipe = {
        'id': ObjectId(file_id),
        'user': user,
        'album': str(album)
    }

    record = get_state_single(pipe)

    if record is not None and 'gender' in record:
        return str(record['gender'])
    else:

        gender = str(get_gender_prediction(filename, method)).lower()

        return gender


def get_type_face(user, album, filename, method):

    file_id = ObjectId(os.path.splitext(filename)[0])

    pipe = {
        'id': ObjectId(file_id),
        'user': user,
        'album': str(album)
    }

    record = get_state_single(pipe)

    if record is not None and 'type_faces' in record:
        type_faces = record.get('type_faces')

        return str(type_faces)
    else:

        # get predicted face type

        type_faces = get_face_type(subject_id=filename, method=method)

        if type_faces is not None:

            return str(type_faces).lower()


def get_manual_age(user, filename, album):
    file_id = ObjectId(os.path.splitext(filename)[0])

    pipe = {
        'id': ObjectId(file_id),
        'user': user,
        'album': str(album)
    }

    record = get_state_single(pipe)

    if record is not None and 'manual_age' in record:
        return int(record['manual_age'])
    else:
        return int(album)


def prepare_data(f):

    dict_azure_info = get_azure_info(f)
    dict_flickr_info = get_flickr_info(f)

    dict_image_info = {'_id': os.path.splitext(f)[0]}

    if dict_azure_info:
        dict_image_info.update(dict_azure_info)

    if dict_flickr_info:
        dict_image_info.update(dict_flickr_info)

    # censor sensitive information

    if dict_image_info and dict_flickr_info:
        dict_image_info = censor_sensitive_information(dict_image_info)

    return dict_image_info


def prepare_data_json(f):
    dict_azure_info = get_azure_info(f)
    dict_flickr_info = get_flickr_info(f)

    dict_image_info = {'_id': os.path.splitext(f)[0]}

    if dict_azure_info:
        dict_image_info.update(dict_azure_info)

    if dict_flickr_info:
        dict_image_info.update(dict_flickr_info)

    # censor sensitive information

    if dict_image_info and dict_flickr_info:
        dict_image_info = censor_sensitive_information(dict_image_info)

    return dict_image_info


def get_face_rectangles(image_id):

    filename = str(image_id) + config.IMAGE_EXTENSION
    dict_info = get_azure_info(filename)
    list_faces = []

    if dict_info.get('dlib_service') is not None:
        list_service = dict_info.get('dlib_service')

        for i in list_service.get('faceRectangle'):
            list_faces.append(i)

    elif dict_info.get('azure_service') is not None:
        list_service = dict_info.get('azure_service')

        for i in list_service:
            list_faces.append(i.get('faceRectangle'))

    return list_faces


def get_original_image_url(image_id):

    record = cl_image_data.find_one({
        '_id': image_id
    })

    if record is not None:
        return record.get('url_o')


def get_user(image_id):
    record = cl_image_data.find_one({
        '_id': image_id
    })

    owner = record.get('owner')

    if owner is not None:
        return owner.get('nsid')