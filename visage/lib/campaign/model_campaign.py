from datetime import datetime
from bson import ObjectId
import config
from db_config import client_db
from visage.lib import database_manager
from visage.lib.database_manager import get_images_detail
from visage.lib.deleted import model_deleted
from visage.lib.resolve.model_resolve import Resolve
from visage.lib.vote import model_vote
from visage.lib.vote.model_vote import Vote

db_name = 'YAHOO_REST'

cl_campaign_transition = client_db[db_name]["campaign_transition"]
cl_campaign = client_db[db_name]["campaign"]
cl_campaign_pending = client_db[db_name]["campaign_pending"]


class Campaign:
    def __init__(self, campaign_id=None, age=None, gender=None, type_faces=None):

        if campaign_id and not age and not gender and not type_faces:

            if ObjectId.is_valid(campaign_id):

                campaign_record = get_campaign(campaign_id)

                if campaign_record:
                    age = campaign_record.get('age')
                    gender = campaign_record.get('gender')
                    type_faces = campaign_record.get('type_faces')

                else:
                    print('error with data')

        self.age = int(age) if age else None
        self.gender = gender
        self.type_faces = type_faces
        self.client_campaign = cl_campaign
        self.id = ObjectId(campaign_id) if ObjectId.is_valid(campaign_id) else None
        self.user = None

    def get_user(self):
        return self.user

    def set_user(self, user):
        self.user = user

    def get_client_campaign(self):
        return self.client_campaign

    def get_id(self):
        return ObjectId(self.id)

    def set_id(self):

        # get code, if it exists return code
        # else insert campaign to collection and return code

        campaign_record = self._get_campaign()

        if campaign_record:
            campaign_id = campaign_record.get('_id')
            self.id = campaign_id
        else:
            campaign_record = self._insert_campaign()
            self.id = campaign_record

    def get_age(self):

        if self.age:
            return int(self.age)

    def get_gender(self):
        return self.gender

    def get_type_faces(self):
        return self.type_faces

    def _get_campaign(self):

        pipe = {
            'age': self.get_age(),
            'gender': self.get_gender(),
            'type_faces': self.get_type_faces()
        }

        return self.get_client_campaign().find_one(pipe)

    def _insert_campaign(self):
        pipe = {
            'age': self.get_age(),
            'gender': self.get_gender(),
            'type_faces': self.get_type_faces()
        }

        _id = self.get_client_campaign().insert_one(pipe)

        return _id.inserted_id

    def get_campaign(self):

        pipe = {
            '_id': ObjectId(self.get_id())
        }

        return self.get_client_campaign().find_one(pipe)

    def set_last_access(self):
        self.get_client_campaign().update_one(
            {'_id': self.get_id()},
            {'$set': {'last_access': datetime.now()}}, upsert=False
        )

    def insert_positive_images(self, images):

        self.get_client_campaign().update_one(
            {
                '_id': self.get_id()
            },
            {
                '$set': {'positive': images}
            },
            upsert=False
        )

    def insert_disagreed_images(self, images):

        self.get_client_campaign().update_one(
            {
                '_id': self.get_id()
            },
            {
                '$set': {'disagreed': images}
            },
            upsert=False
        )

    def get_positive_files(self, votes=None, user_check=None):

        campaigns = self.get_client_campaign().find_one(
            {
                '_id': self.get_id()
            }
        )

        if campaigns is not None:

            list_files = []

            if votes < 3:
                # get vote

                v = Vote(self.get_user(), self.get_campaign())

                cursor = v.get_cursor_positive_votes(
                    number_votes=votes,
                    age=self.get_age(),
                    gender=self.get_gender(),
                    user_check=user_check
                )

                if cursor is not None:

                    # if value is in resolution collection and gender is the same, add to list

                    r = Resolve(self.get_campaign(), self.get_user())

                    for x in cursor:

                        record = r.get_client_resolve().find_one(
                            {
                                'id': ObjectId(x.get('_id')),
                                'gender': self.get_inverse_gender_campaign().get_gender()
                            }
                        )

                        if record is None:
                            list_files.append(str(x.get('_id')) + config.IMAGE_EXTENSION)
            else:

                list_file_objects = campaigns.get('positive')

                # get deleted files

                deleted = model_deleted.get_deleted()

                if list_file_objects is not None and len(list_file_objects) > 0:
                    list_files = [str(x) + config.IMAGE_EXTENSION for x in list_file_objects if x not in deleted]

            return list_files

    def get_disagreed_files(self):

        list_files = []

        campaigns = self.get_client_campaign().find_one(
            {
                '_id': self.get_id()
            }
        )

        if campaigns is not None:

            list_file_objects = campaigns.get('disagreed')[:config.THUMBNAILS_PER_PAGE]

            if list_file_objects:
                list_files = [str(x) + config.IMAGE_EXTENSION for x in list_file_objects]

        return list_files

    def delete_disagreed_files(self, images):

        # deletes disagreed files from array and returns size of array

        self.get_client_campaign().update_one(
            {
                '_id': self.get_id()
            },
            {
                '$pull': {'disagreed': {'$in': images}}
            },
            upsert=False
        )

        campaign_record = self.get_client_campaign().find_one(
            {
                '_id': self.get_id()
            }
        )

        array_disagreed = campaign_record.get('disagreed')

        if array_disagreed is not None:
            return len(array_disagreed)

    def get_positive_unique_subjects(self):

        list_subjects = self.get_positive_files()

        records = get_images_detail(list_subjects)

        return records

    def get_inverse_gender_campaign(self):

        new_gender = get_inverse_gender(self.get_gender())

        new_campaign = Campaign(age=self.get_age(), gender=new_gender, type_faces='single')
        new_campaign.set_id()

        return new_campaign

    def change_inaccurate_gender(self, file_name, new_gender):

        new_campaign = Campaign(age=self.get_age(), gender=new_gender, type_faces='single')
        new_campaign.set_id()

        # get votes for file_name and change gender

        model_vote.cl_vote.update_many(
            {
                'id': ObjectId(file_name)
            },
            {
                '$set': {
                    'gender': new_gender,
                }
            }, upsert=False
        )

        # remove item from the positive field

        self.get_client_campaign().update_one(
            {
                '_id': ObjectId(self.get_id())
            },
            {
                '$pull': {'positive': ObjectId(file_name)}
            },
            upsert=False
        )

        # add item to positive field to correct gender

        self.get_client_campaign().update_one(
            {
                '_id': ObjectId(new_campaign.get_id())
            },
            {
                '$push': {'positive': ObjectId(file_name)}
            },
            upsert=False
        )

    def get_pending_campaign_number(self):
        record = cl_campaign_pending.find_one(
            {
                'campaign_id': self.get_id(),
                'user': self.get_user()
            }
        )

        if record is not None:
            return record.get('number_images')


def get_inverse_gender(gender):
    new_gender = 'male' if str(gender) == 'female' else 'female'

    return new_gender


def change_gender(file_name):
    """
Some images that were voted as females belonged initially to a male campaign (and the other way round) due to a
misclassified Azure prediction. In order to fix this, we have to obtain the current campaign id, remove the item from
the 'positive' campaign subfield in MongoDB in the table campaign, obtain the campaign id for the correct gender and
add the value to the 'positive' campaign subfield corresponding to the fixed gender.
*Update: change the values of the individual votes first!
    :param file_name: name of the file for which we intend to change the gender
    """

    # get current campaign id from the positive field

    cursor = cl_campaign.find_one({'positive': ObjectId(file_name)})

    if cursor is not None:
        campaign_id = cursor.get('_id')

        if campaign_id is not None:
            # get correct gender

            campaign = Campaign(campaign_id=campaign_id)

            gender = campaign.get_inverse_gender_campaign().get_gender()

            campaign.change_inaccurate_gender(file_name, gender)

            return gender


def get_positive_files_all(rectangles, unique):
    pipe = {
        'positive': {'$exists': True}, '$where': 'this.positive.length>0'
    }

    records = cl_campaign.find(pipe)

    dict_files = {}

    from flask import request

    host = request.host

    for i in records:
        list_file_objects = i.get('positive')

        for j in list_file_objects:

            # get url of image from server.... think about flickr in the future

            age_campaign = i.get('age')
            gender_campaign = i.get('gender')

            # get_original_flickr_url = database_manager.get_original_image_url(j)

            # if get_original_flickr_url is not None:
            #    url = get_original_flickr_url
            if False:
                pass
            else:
                url = 'http://{0}{1}/image/{2}/{3}.jpg/{4}/'.format(
                    host,
                    config.FRONTEND_PREFIX,
                    age_campaign,
                    str(j),
                    'full'
                )

            pipe = {
                'age': age_campaign,
                'gender': gender_campaign,
                'url': url
            }

            if rectangles is not None:
                face_rectangles = database_manager.get_face_rectangles(j)

                # change here this method

                pipe.update({'face_rectangles': face_rectangles})

            if not unique:
                dict_files[str(j)] = pipe
            else:
                user = database_manager.get_user(j)
                pipe.update({'user': user})

                # restrict only 1 image per age per user

                # get list by user

                images_user = [item for item in dict_files.values() if
                               item.get('user') == user and item.get('age') == age_campaign]

                if len(images_user) is 0:
                    dict_files[str(j)] = pipe

    return dict_files


def get_campaign(campaign_id):
    if ObjectId.is_valid(campaign_id):
        pipe = {
            '_id': ObjectId(campaign_id)
        }

        return cl_campaign.find_one(pipe)


def get_campaigns(n):
    cursor = cl_campaign.find({'last_access': {'$exists': True}}).limit(n).sort('last_access', direction=-1)

    return list(cursor)


def get_all_campaigns():
    cursor = cl_campaign.find(
        {
            #'type_faces': {'$ne': None},
            'type_faces': {'$eq': 'single'},
            'age': {'$ne': None}
        }).sort('age', direction=1)

    return list(cursor)


def get_campaign_transition(age, gender, number_votes):
    if int(number_votes) == config.NUMBER_VOTES:
        comparison_function = '$eq'
    else:
        comparison_function = '$gte'

    pipe = {
        'age': age,
        'gender': gender,
        'count': {comparison_function: number_votes}
    }

    record = cl_campaign_transition.find(
        pipe
    )

    return record


def delete_campaign_transition(file_name):
    cl_campaign_transition.remove(
        {
            '_id': ObjectId(file_name)
        }
    )


def get_campaign_id(image_id):
    if ObjectId.is_valid(image_id):

        record = cl_campaign.find_one({
            '$or': [{'positive': ObjectId(image_id)}, {'disagreed': ObjectId(image_id)}]
        })

        if record is not None:
            return record.get('_id')
        else:

            # get image from data table, so refer to pertinent class

            record = database_manager.cl_images_created.find_one(
                {
                    'id': ObjectId(image_id)
                }
            )

            if record is not None:
                # get age

                age = record.get('age')
                azure_services = record.get('azure_service')

                if azure_services is not None:
                    for v in azure_services:
                        face_attributes = v.get('faceAttributes')

                        if face_attributes is not None:
                            gender = face_attributes.get('gender')

                            # get campaign id for each gender

                            campaign_result = Campaign(age=age, gender=gender, type_faces='single')
                            campaign_result.set_id()
                            return campaign_result.get_id()


def update_pending_table(dict_values, user):
    # drop table then insert new values

    cl_campaign_pending.delete_many(
        {
            'user': user
        }
    )

    cl_campaign_pending.insert_many(dict_values)


def update_pending_table_campaign(number_images, user, campaign_id):
    my_query = {"campaign_id": ObjectId(campaign_id), "user": user}
    new_values = {"$set": {"number_images": number_images}}

    cl_campaign_pending.update_one(my_query, new_values)
