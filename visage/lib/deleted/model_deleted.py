from bson import ObjectId

from db_config import client_db

cl_data = client_db["YAHOO_REST"]["data"]
cl_deleted = client_db["YAHOO_REST"]["deleted"]


def get_deleted():
    list_deleted = []

    deleted = cl_deleted.find(
        {

        }
    )

    for item in deleted:
        list_deleted.append(item.get('_id'))

    return list_deleted


def check_deleted(file_name):
    item = cl_deleted.find_one(
        {
            '_id': ObjectId(file_name)
        }
    )

    if item is not None:
        return True
    else:
        return False


def delete_item(file_name, user):
    item = cl_deleted.find_one({'_id': ObjectId(file_name)})

    if item is None:
        cl_deleted.insert(
            {
                '_id': ObjectId(file_name),
                'user': user
            }
        )
    else:
        print('file {} already deleted'.format(file_name))
