import os

from bson import ObjectId

from visage.lib.database_manager import get_image_info, get_face_type, get_azure_info


class Image(object):
    def __init__(self, album, filename, config, method):
        self.album = album
        self.filename = filename
        self.type_faces = get_face_type(filename, method)
        self.config = config
        self.method = method

    def get_method(self):
        return self.method

    def get_type(self):
        return self.type_faces

    def get_config(self):
        return self.config

    @property
    def orig_dir(self):
        return self._get_dir('ALBUMS')

    @property
    def orig_file(self):
        return os.path.join(self.orig_dir, self.filename)

    @property
    def edit_dir(self):
        return self._get_dir('EDITS')

    @property
    def edit_file(self):
        return os.path.join(self.edit_dir, self.filename)

    @property
    def exif_dir(self):
        return os.path.join(self._get_dir('CACHE'), 'exif')

    @property
    def exif_file(self):
        return os.path.join(self.exif_dir, self.filename + '.exif')

    def _get_dir(self, kind):
        # return os.path.join(self.config[kind + '_DIR'], self.album, self.get_type())
        return os.path.join(self.config[kind + '_DIR'], self.album)

    def is_edited(self):
        if os.path.exists(self.edit_file):
            return True
        else:
            return False

    def get_edit_or_original_dir(self):
        if self.is_edited():
            return self.edit_dir
        else:
            return self.orig_dir

    def get_fullsize_path(self):
        return os.path.join(
            self.get_edit_or_original_dir(),
            self.filename
        )

    def get_filename(self):
        return self.filename

    def get_face_rectangle_coordinates(self):

        def get_rectangle(face_dictionary):

            # this for Azure

            if face_dictionary.get('faceRectangle'):
                rect = face_dictionary['faceRectangle']
                left = rect['left']
                top = rect['top']
                bottom = left + rect['height']
                right = top + rect['width']
                return (left, top), (bottom, right)
            else:

                # this for dlib

                left = face_dictionary.get('left')
                top = face_dictionary.get('top')
                bottom = left + face_dictionary.get('height')
                right = top + face_dictionary.get('width')
                return (left, top), (bottom, right)

        face_rectangle = []

        records = get_azure_info(self.get_filename())

        if self.get_method() == 'azure':

            # first try dlib, eventually change this to dlib

            if records.get('dlib_service') is not None:
                # get from dlib instead

                dlib_service = records.get('dlib_service')

                if dlib_service.get('faceRectangle') is not None:
                    fr = dlib_service.get('faceRectangle')

                    for faces in fr:
                        face_rectangle.append(get_rectangle(faces))

                return face_rectangle

            elif records is not None and records.get('azure_service'):
                azure_service = records.get('azure_service')

                # get all rectangles

                if azure_service is not None and len(azure_service) > 0:
                    for faces in azure_service:
                        if faces.get('faceRectangle') is not None:
                            face_rectangle.append(get_rectangle(faces))

                    return face_rectangle

    def get_title(self):

        records = get_image_info(self.get_filename())

        if records is not None and records.get('title'):
            title = records.get('title').get('_content')

            return title

    def get_description(self):

        records = get_image_info(self.get_filename())

        if records is not None and records.get('description'):
            description = records.get('description').get('_content')

            return description

    def get_tags(self):

        records = get_image_info(self.get_filename())

        tag_list = []

        if records is not None and records.get('tags'):

            # get tags

            tags = records.get('tags')

            tag = tags.get('tag')

            if tag:
                for t in tag:
                    if t.get('raw'):
                        tag_list.append(t.get('raw'))

        return tag_list

    def get_url(self):

        records = get_image_info(self.get_filename())

        if records is not None and records.get('urls'):

            urls = records.get('urls')

            if urls.get('url'):
                for url in urls.get('url'):
                    if url.get('_content'):
                        return url.get('_content')

    def get_date(self):

        file_name, extension = os.path.splitext(self.get_filename())

        file_name_oi = ObjectId(file_name)

        return file_name_oi.generation_time
