from heapq import nlargest

import config
from db_config import client_db
from visage.lib.state.model_state import State

cl_vote = client_db["YAHOO_REST"]["vote"]


class Vote:
    def __init__(self, user, campaign):
        self.user = user
        self.campaign = campaign
        self.number_votes = config.NUMBER_VOTES
        self.client_vote = cl_vote

    def get_client_vote(self):
        return self.client_vote

    def get_campaign(self):
        return self.campaign

    def get_user(self):
        return self.user

    def set_user(self, user):
        self.user = user

    def get_number_votes(self):
        return self.number_votes

    def set_number_votes(self, number_votes):
        self.number_votes = number_votes

    def submit_vote(self):

        state = State(user=self.get_user(), campaign=self.get_campaign())

        cursor = state.get_state()

        for rec in cursor:

            gender_record = rec.get('gender')
            face_record = rec.get('type_faces')

            age_campaign = self.get_campaign().get_age()
            gender_campaign = self.get_campaign().get_gender()

            pipe = {
                'id': rec.get('id'),
                'user': self.get_user(),
                'campaign_id': self.get_campaign().get_id(),
                'gender': gender_record,
                'age_campaign': age_campaign,
                'gender_campaign': gender_campaign,
                'type_faces': face_record
            }

            if rec.get('manual_age') is not None:
                pipe.update(
                    {'manual_age': int(rec.get('manual_age'))}
                )

            number_votes = cl_vote.count(
                {
                    'id': rec.get('id'),
                    'campaign_id': self.get_campaign().get_id(),
                }
            )

            pipe_no_duplicates = {
                'id': rec.get('id'),
                'user': self.get_user(),
                'campaign_id': self.get_campaign().get_id()
            }

            if cl_vote.find_one(pipe_no_duplicates):
                print('you have already voted', pipe_no_duplicates)

            elif number_votes < self.get_number_votes():
                pipe.update({'state': 'yes' if rec.get('state') == 'publish' else 'no'})
                cl_vote.insert(pipe)

            elif number_votes == self.get_number_votes():
                print('Votes already reached maximum, not voting', pipe)
            else:
                print('Votes over limit', pipe)

        state.delete_records()

    def filter_voted(self):

        list_voted = []

        pipe = {
            'user': self.get_user(),
            'campaign_id': self.get_campaign().get_id()
        }

        cursor = cl_vote.find(
            pipe
        )

        for c in cursor:
            list_voted.append(c['id'])

        return list_voted

    def filter_voted_all(self):
        """
    Filter votes that have reached 3 times
    or 2 negative votes
        :return:
        """
        # get images that have been voted 3 times

        list_voted = []

        pipe = [
            {
                '$match': {'campaign_id': self.get_campaign().get_id()}
            },
            {
                '$group': {'_id': "$id", 'count': {'$sum': 1}}
            },
            {
                '$match': {'count': {'$gte': self.get_number_votes()}}
            }
        ]

        cursor = cl_vote.aggregate(
            pipe
        )

        for c in cursor:
            list_voted.append(c['_id'])

        return list_voted

    def get_negative_votes(self, number_votes):

        list_voted = []

        pipe = [
            {
                '$match': {
                    'campaign_id': self.get_campaign().get_id(),
                    'state': 'no'
                }
            },
            {
                '$group': {'_id': "$id", 'count': {'$sum': 1}}
            },
            {
                '$match': {
                    'count': {'$eq': number_votes}
                }
            }
        ]

        cursor = cl_vote.aggregate(
            pipe
        )

        for c in cursor:
            list_voted.append(c['_id'])

        return list_voted

    def filter_voted_inverse_gender(self):

        new_campaign = self.get_campaign().get_inverse_gender_campaign()

        # get campaign for other gender

        list_voted = []

        pipe = {
            'user': self.get_user(),
            'campaign_id': new_campaign.get_id()
        }

        cursor = cl_vote.find(
            pipe
        )

        for c in cursor:
            list_voted.append(c['id'])

        return list_voted

    def get_cursor_positive_votes(self, number_votes, age, gender, user_check):

        if number_votes == config.NUMBER_VOTES:
            comparison_function = '$eq'
        else:
            comparison_function = '$gte'

        pipe_filter = {}

        if user_check is True:

            pipe_filter = {
                '$and': [{'state': 'yes'}, {'age_campaign': age}, {'gender': gender}, {'user': self.get_user()}]
            }

        elif user_check is False:

            pipe_filter = {
                '$and': [{'state': 'yes'}, {'age_campaign': age}, {'gender': gender}]
            }
        else:

            # todo here

            print('parameter not specified')

        pipe = [
            {
                '$match':
                    pipe_filter
            },
            {
                '$group':
                    {
                        '_id': '$id',
                        'count': {'$sum': 1}
                    }
            },
            {
                '$match':
                    {
                        'count': {comparison_function: number_votes}
                    }
            },

            {
                '$sort': {"_id": -1}
            }
        ]

        cursor = self.get_client_vote().aggregate(
            pipe
        )

        return cursor


def get_age_gender(image_id):
    record = cl_vote.find_one(
        {
            'id': image_id
        }
    )

    # todo gender not reliable yet,

    return record.get('age_campaign'), record.get('gender')

    # leg this

    if records.count() == 3:
        if all(r.get('gender') == gender for r in records):
            return gender


def get_top_users(number_users):
    cursor = cl_vote.aggregate(
        [
            {
                '$group':
                    {
                        '_id': '$user',
                        'count': {'$sum': 1}
                    }
            }
        ]
    )

    list_users = list(cursor)

    dict_user_sum = {}

    for l in list_users:
        dict_user_sum[l.get('_id')] = {'count': l.get('count')}

    highest = nlargest(number_users, dict_user_sum, key=dict_user_sum.get)

    return highest
