from bson import ObjectId
import config
from db_config import client_db
from visage.lib.campaign import model_campaign
from visage.lib.database_manager import prepare_data

cl_data = client_db["YAHOO_REST"]["data"]
cl_image_created = client_db["YAHOO_REST"]["image_created"]


class Photo:
    def __init__(self, image_id):
        self.id = ObjectId(image_id)
        self.collection_data = cl_data
        self.collection_image_created = cl_image_created

    def get_id(self):
        return self.id

    def get_collection_data(self):
        return self.collection_data

    def get_collection_image_created(self):
        return self.get_collection_data()

    def get_info(self):
        # get azure info

        filename = str(self.get_id()) + config.IMAGE_EXTENSION

        dict_prepare_data = prepare_data(filename)

        return dict_prepare_data


def get_data(rectangles, unique):
    positive_files = model_campaign.get_positive_files_all(rectangles=rectangles, unique=unique)

    # replace values from resolved files (much more reliable)

    return positive_files
