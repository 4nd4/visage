==============================
VisAGe: Pre-teen Face Dataset
==============================
:Author: Felix Anda

VisAGe is a web-based voting system aimed to label images with age and gender.

VisAGe was based on Showoff that was written in Python_, utilizing Flask_. The frontend uses Bootstrap_,
jQuery_ and Swipebox_.

Who to contact:

Visage_

Copyright and license
---------------------

:copyright: (c) 2010-2014 by Jochem Kossen <jochem@jkossen.nl>
:license: two-clause BSD

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

   1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.

   2. Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

VisAGe features and lack thereof
----------------------------------

* Non-destructive: VisAGe does not change original files. All modifications
  are done on copies in the cache

* Image albums are just directories with image files, VisAGe will separate files by age 
  and in single, multiple and other folders for faces detected.

* MongoDB is requiered

* Theme support (Jinja2 is used for templates)

Installation
------------

Requirements
~~~~~~~~~~~~
VisAGe requires the following software:

* `Python`_
* `Flask`_
* `Flask-WTF`_
* `py-bcrypt`_
* `Werkzeug`_
* `Jinja2`_
* `Pillow`_

VisAGe comes with a setup.py installation script which uses setuptools.  The
following command will install showoff and its dependencies:

::

    $ python setup.py install

You can also use pip to install the dependencies, but then you need to register
the showoff installation manually (add the path to the showoff parent dir to a
file called showoff.pth inside your site-packages directory):

::

    $ pip install -r requirements.txt


Installation Instructions
~~~~~~~~~~~~~~~~~~~~~~~~~
* Install the software listed under `Requirements`_
* Adjust the configuration in config.py
* Make sure the CACHE_DIR is writable
* Make sure the EDITS_DIR and SHOWS_DIR are readable by the viewer app

I recommend using a combination of uwsgi and nginx. See the examples dir for
relevant wsgi files.

Check http://flask.pocoo.org/docs/deploying/ for more information
concerning deployment.

.. _Python: http://www.python.org
.. _Flask: http://flask.pocoo.org
.. _Flask-WTF: https://flask-wtf.readthedocs.org/en/latest/
.. _Pillow: https://pillow.readthedocs.org/en/latest/
.. _py-bcrypt: http://www.mindrot.org/projects/py-bcrypt/
.. _jQuery: http://jquery.com/
.. _Bootstrap: http://getbootstrap.com
.. _Swipebox: http://brutaldesign.github.io/swipebox/
.. _Werkzeug: http://werkzeug.pocoo.org
.. _Jinja2: http://jinja.pocoo.org
.. _Visage: https://forensicsandsecurity.com/

Apache Configuration
~~~~~~~~~~~~
In order to configure the app with apache, we must modify do the following in our visage.wsgi file:

* modify the path to our activate_this.py file from our virtual environment
* modify the path of our application:

::

   import sys
   sys.path.insert(0, '/path/to/the/application')

After, create a copy of 000-default.config typically found in the folder /etc/apache2/sites-available/. Create a copy called visage.config.

Edit the file to look like this:

::

   <VirtualHost *>
      ServerName example.com
      WSGIDaemonProcess yourapplication user=user1 group=group1 threads=5
      WSGIScriptAlias / /var/www/yourapplication/yourapplication.wsgi
      
      <Directory /var/www/yourapplication>
         WSGIProcessGroup yourapplication
         WSGIApplicationGroup %{GLOBAL}
         Order deny,allow
         Allow from all
      </Directory>
   </VirtualHost>


Refer to http://flask.pocoo.org/docs/0.12/deploying/mod_wsgi/#installing-mod-wsgi
