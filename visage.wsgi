import sys

activate_this = '/home/santiago/.virtualenvs/visage/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

sys.path.insert(0, '/home/santiago/Documents/projects/visage')

from run_frontend import app as application