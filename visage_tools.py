import io
import os
import dlib as dlib
import numpy as np
import requests
from PIL import Image, ImageFile
from bson import ObjectId

import config
import db_config
from visage.lib import database_manager, image
from visage.lib.campaign.model_campaign import Campaign

cl_vote = db_config.client_db["YAHOO_REST"]["vote"]
cl_campaign = db_config.client_db["YAHOO_REST"]["campaign"]
cl_campaign_transition = db_config.client_db["YAHOO_REST"]["campaign_transition"]
cl_toggle = db_config.client_db["YAHOO_REST"]["toggle_state"]
cl_data = db_config.client_db["YAHOO_REST"]["data"]
cl_resolve = db_config.client_db["YAHOO_REST"]["resolve"]
cl_image_created = db_config.client_db["YAHOO_REST"]["image_created"]

# get records that have been changed the gender


def campaign_vote_transition():

    vote_record = cl_vote.find({})

    counter = 0

    for rec in vote_record:

        campaign = Campaign(rec.get('campaign_id'))

        gender_campaign = campaign.get_gender()

        gender_voted = rec.get('gender')

        if gender_campaign != gender_voted:

            pipe_transition = {
                '_id': rec.get('id')
            }

            transition_record = cl_campaign_transition.find_one(pipe_transition)

            if not transition_record:
                result = cl_campaign_transition.insert(
                    {
                        '_id': ObjectId(rec.get('id')),
                        'age': campaign.get_age(),
                        'gender': gender_voted,
                        'count': 1,
                        'users': [rec.get('user')],
                        'campaign_previous': campaign.get_id()
                    }
                )
                print('record inserted', result)

            else:

                transition_counter = int(transition_record.get('count'))

                if transition_counter <= config.NUMBER_VOTES and len(transition_record.get('users')) <= config.NUMBER_VOTES:

                    user_list = list(transition_record.get('users'))

                    if transition_counter < config.NUMBER_VOTES and rec.get('user') not in user_list:

                        if rec.get('user') not in user_list:

                            user_list.append(rec.get('user'))

                            transition_counter += 1

                            update_pipe = {'count': transition_counter, 'users': user_list}

                            cl_campaign_transition.update_one(
                                pipe_transition,
                                {
                                    '$set': update_pipe
                                }, upsert=False
                            )

                    if transition_counter == config.NUMBER_VOTES and len(user_list) == config.NUMBER_VOTES:

                        new_campaign = Campaign(
                            age=campaign.get_age(),
                            gender=gender_voted,
                            type_faces=campaign.get_type_faces())

                        new_campaign.set_id()

                        check_new_campaign = cl_campaign_transition.find_one(
                            {
                                '_id': rec.get('id'),
                                'new_campaign': {'$exists': False}
                            }
                        )

                        if check_new_campaign:

                            update_pipe = {
                                'count': transition_counter,
                                'users': user_list,
                                'campaign_new': new_campaign.get_id()
                            }

                            cl_campaign_transition.update_one(
                                pipe_transition,
                                {
                                    '$set': update_pipe
                                }, upsert=False
                            )

                            # copy value to campaign

                            # check if value exists

                            campaign_record = cl_campaign.find_one({'_id': new_campaign.get_id()})

                            list_positive = campaign_record.get('positive')

                            if list_positive:
                                if transition_record.get('_id') not in list_positive:

                                    list_positive.append(transition_record.get('_id'))

                                    cl_campaign.update(
                                        {
                                            '_id': new_campaign.get_id()
                                        },
                                        {
                                            '$set': {'positive': list_positive}
                                        }

                                    )
        else:
            counter += 1
            print(counter)


def fix_toggle_table():

    # fsanda

    # this deletes votes that have already been submitted

    toggle_records = cl_toggle.find({})

    for tr in toggle_records:
        user = tr.get('user')
        subject_id = tr.get('id')
        campaign_id = tr.get('campaign_id')

        # if it has been voted and remains in toggle, remove

        vote_record = cl_vote.find_one(
            {
                'user': user,
                'id': subject_id,
                'campaign_id': campaign_id
            }
        )

        if vote_record:
            print('deleting toggle item', subject_id)

            remove_result = cl_toggle.remove(
                {
                    'id': vote_record.get('id'),
                    'campaign_id': vote_record.get('campaign_id')
                }
            )

            print(remove_result)


def get_original_images(age, gender):
    campaign = Campaign(age=age, gender=gender, type_faces='single')
    campaign.set_id()

    files = campaign.get_positive_files()

    if files:

        for fi in files:

            # get stuff

            record = cl_data.find_one({
                '_id': ObjectId(os.path.splitext(fi)[0])
            })

            import urllib

            path_directory = os.path.expanduser(config.ALBUMS_DIR + 'original/' + str(gender).upper() + str(age) + '/')

            if not os.path.exists(path_directory):
                os.makedirs(path_directory)

            url = record.get('url_o')

            if url is not None:

                f = open(path_directory + fi, 'wb')

                f.write(urllib.urlopen(record.get('url_o')).read())
                f.close()
            else:
                print(record.get('_id'), 'original not found')


def get_images_range(minimum, maximum):
    for i in range(minimum, maximum):
        get_original_images(age=i, gender='male')

    for i in range(minimum, maximum):
        get_original_images(age=i, gender='female')
        print()


def get_age_gender(campaign_id):
    record_campaign = cl_campaign.find_one(
        {
            '_id': ObjectId(campaign_id)
        }

    )

    return record_campaign.get('age'), record_campaign.get('gender')


def fix_campaign():
    # add age_campaign and gender_campaign to table

    i = 0

    counter = cl_vote.count()

    while i < counter:

        record_vote = cl_vote.find_one(
            {
                'age_campaign': {'$exists': False},
                'gender_campaign': {'$exists': False}
            }
        )

        if record_vote is not None:

            campaign_id = record_vote.get('campaign_id')

            age, gender = get_age_gender(campaign_id)

            cl_vote.update(
                {
                    '_id': record_vote.get('_id')
                },
                {
                    '$set': {
                        'age_campaign': age,
                        'gender_campaign': gender
                    }
                }
            )

            print('updated', i)

            i += 1

        else:
            break


def fix_resolved():

    i = 0

    counter = cl_resolve.count()

    while i < counter:

        record_resolve = cl_resolve.find_one(
            {
                'age_campaign': {'$exists': False},
            }
        )

        if record_resolve is not None:

            campaign_id = record_resolve.get('campaign_id')

            age, gender = get_age_gender(campaign_id)

            cl_resolve.update(
                {
                    '_id': record_resolve.get('_id')
                },
                {
                    '$set': {
                        'age_campaign': age
                    }
                }
            )

            print('updated', i)

            i += 1

        else:
            break


def fix_downloads():

    # do the same as the web method but instead, fix

    pipe = {
        'positive': {'$exists': True}, '$where': 'this.positive.length>0'
    }

    records = cl_campaign.find(pipe)

    # dict_files = {}

    # host = request.host

    counter = 0

    for i in records:
        list_file_objects = i.get('positive')

        for j in list_file_objects:

            # get url of image from server.... think about flickr in the future

            record_update = cl_image_created.find_one(
                {
                    'id': j
                }
            )

            if record_update.get('fix') is not None or record_update.get('dlib_service') is not None:

                print(record_update.get('_id'), 'done')

                continue

            age_campaign = i.get('age')
            # gender_campaign = i.get('gender')

            get_original_flickr_url = database_manager.get_original_image_url(j)

            # update collection so it doesn't get evaluated twice

            id_update = record_update.get('_id')

            cl_image_created.update_one(
                {
                    '_id': id_update
                },
                {
                    '$set': {
                        'fix': True
                    }
                }, upsert=False
            )

            if get_original_flickr_url is not None:
                url = get_original_flickr_url

                url_extension = os.path.splitext(url)[1]

                print(url)

                # compare images from url and local url

                data_url = requests.get(url).content

                try:

                    img_data = Image.open(io.BytesIO(data_url))
                except Exception as ex:
                    print(ex.message)
                    continue

                local_image_path_without_extension = os.path.join(
                    config.ALBUMS_DIR,
                    str(age_campaign),
                    str(j))

                if os.path.exists(local_image_path_without_extension + str(url_extension)):
                    local_image_path = local_image_path_without_extension + str(url_extension)
                    img_local_data = Image.open(local_image_path)
                elif os.path.exists(local_image_path_without_extension + str(config.IMAGE_EXTENSION)):
                    local_image_path = local_image_path_without_extension + str(config.IMAGE_EXTENSION)
                    img_local_data = Image.open(local_image_path)
                else:
                    print('file not found')
                    continue

                if img_local_data.size != img_data.size:

                    local_image_path = local_image_path_without_extension + str(url_extension)

                    # get biggest image

                    width_url, height_url = img_data.size
                    width_local_url, height_local_url = img_local_data.size

                    if width_url > width_local_url and height_url > height_local_url:

                        # get coordinates

                        # visage_image = image.Image(age_campaign, str(j) + config.IMAGE_EXTENSION, config, 'azure')

                        # list_coordinates_rectangle = visage_image.get_face_rectangle_coordinates()

                        # get coordinates from DLIB

                        ImageFile.LOAD_TRUNCATED_IMAGES = True

                        gray = img_data.convert('L')

                        detector = dlib.get_frontal_face_detector()

                        faces_hog = detector(np.array(gray), 1)

                        list_dlib_faces = []

                        for face in faces_hog:
                            x = face.left()
                            y = face.top()
                            w = face.right() - x
                            h = face.bottom() - y

                            # draw = ImageDraw.Draw(img_data)
                            # points = ((x,y), (x + w, y + h))
                            # draw.rectangle(points, outline='green')
                            # img_data.show()

                            # overwrite values in database

                            # add dlib_service field

                            list_dlib_faces.append({

                                    'width': w,
                                    'top': y,
                                    'height': h,
                                    'left': x
                            }
                            )

                        if len(list_dlib_faces) > 0:
                            dlib_service = {
                                'size': {
                                    'width': width_url,
                                    'height': height_url
                                },
                                'faceRectangle': list_dlib_faces
                            }

                            cl_image_created.update_one(
                                {
                                    'id': j
                                },
                                {
                                    '$set': {
                                        'azure_service_size': {'height': height_local_url, 'width': width_local_url},
                                        'dlib_service': dlib_service
                                    }
                                }, upsert=False
                            )

                            # replace image

                            img_local_data.close()
                            img_data.save(local_image_path)  # failing with gif
                            img_data.close()

                            counter += 1

                            print('records affected:', local_image_path)
                            print('number of records affected:', counter)


def fix_image(filename):

    # do the same as the web method but instead, fix

    # get url of image from server.... think about flickr in the future

    j = ObjectId(os.path.splitext(filename)[0])

    record_update = cl_image_created.find_one(
        {
            'id': j
        }
    )

    if record_update.get('fix') is not None or record_update.get('dlib_service') is not None:

        print(record_update.get('_id'), 'done')

    else:

        age_campaign = record_update.get('age')
        # gender_campaign = i.get('gender')

        get_original_flickr_url = database_manager.get_original_image_url(j)

        # update collection so it doesn't get evaluated twice

        id_update = record_update.get('_id')

        cl_image_created.update_one(
            {
                '_id': id_update
            },
            {
                '$set': {
                    'fix': True
                }
            }, upsert=False
        )

        if get_original_flickr_url is not None:
            url = get_original_flickr_url

            url_extension = os.path.splitext(url)[1]

            print(url)

            # compare images from url and local url

            data_url = requests.get(url).content

            try:
                img_data = Image.open(io.BytesIO(data_url))
            except Exception as ex:
                print(ex.message)
                return None

            local_image_path_without_extension = os.path.join(
                config.ALBUMS_DIR,
                str(age_campaign),
                str(j))

            if os.path.exists(local_image_path_without_extension + str(url_extension)):
                local_image_path = local_image_path_without_extension + str(url_extension)
                img_local_data = Image.open(local_image_path)
            elif os.path.exists(local_image_path_without_extension + str(config.IMAGE_EXTENSION)):
                local_image_path = local_image_path_without_extension + str(config.IMAGE_EXTENSION)
                img_local_data = Image.open(local_image_path)
            else:
                print('file not found')
                return None

            if img_local_data.size != img_data.size:

                local_image_path = local_image_path_without_extension + str(url_extension)

                # get biggest image

                width_url, height_url = img_data.size
                width_local_url, height_local_url = img_local_data.size

                if width_url > width_local_url and height_url > height_local_url:

                    # get coordinates from DLIB

                    ImageFile.LOAD_TRUNCATED_IMAGES = True

                    gray = img_data.convert('L')

                    detector = dlib.get_frontal_face_detector()

                    faces_hog = detector(np.array(gray), 1)

                    list_dlib_faces = []

                    for face in faces_hog:
                        x = face.left()
                        y = face.top()
                        w = face.right() - x
                        h = face.bottom() - y

                        # overwrite values in database

                        # add dlib_service field

                        list_dlib_faces.append({

                                'width': w,
                                'top': y,
                                'height': h,
                                'left': x
                        }
                        )

                    if len(list_dlib_faces) > 0:
                        dlib_service = {
                            'size': {
                                'width': width_url,
                                'height': height_url
                            },
                            'faceRectangle': list_dlib_faces
                        }

                        cl_image_created.update_one(
                            {
                                'id': j
                            },
                            {
                                '$set': {
                                    'azure_service_size': {'height': height_local_url, 'width': width_local_url},
                                    'dlib_service': dlib_service
                                }
                            }, upsert=False
                        )

                        # replace image

                        img_local_data.close()
                        img_data.save(local_image_path)  # failing with gif
                        img_data.close()

                        print('record affected:', local_image_path)


def get_rectangle_dlib(age_campaign, filename):

    img_local_data = None

    filename_without_extension = os.path.splitext(filename)[0]

    local_image_path_without_extension = os.path.join(
        config.ALBUMS_DIR,
        str(age_campaign),
        str(filename_without_extension))

    object_id = ObjectId(os.path.splitext(filename)[0])
    local_extension = os.path.splitext(filename)[1]

    get_original_flickr_url = database_manager.get_original_image_url(object_id)

    if get_original_flickr_url is not None:
        url = get_original_flickr_url
        url_extension = os.path.splitext(url)[1]

        local_image_path = local_image_path_without_extension + str(url_extension)
        img_local_data = Image.open(local_image_path)

    else:
        if os.path.exists(local_image_path_without_extension + str(local_extension)):
            local_image_path = local_image_path_without_extension + str(local_extension)
            img_local_data = Image.open(local_image_path)

    if img_local_data is not None:

        width_local_url, height_local_url = img_local_data.size

        ImageFile.LOAD_TRUNCATED_IMAGES = True

        gray = img_local_data.convert('L')

        detector = dlib.get_frontal_face_detector()

        faces_hog = detector(np.array(gray), 1)

        list_dlib_faces = []

        for face in faces_hog:
            x = face.left()
            y = face.top()
            w = face.right() - x
            h = face.bottom() - y

            # overwrite values in database

            # add dlib_service field

            list_dlib_faces.append({

                'width': w,
                'top': y,
                'height': h,
                'left': x
            }
            )

        dlib_service = {
            'size': {
                'width': width_local_url,
                'height': height_local_url
            },
            'faceRectangle': list_dlib_faces
        }

        cl_image_created.update_one(
            {
                'id': object_id
            },
            {
                '$set': {
                    'dlib_service': dlib_service
                }
            }, upsert=False
        )