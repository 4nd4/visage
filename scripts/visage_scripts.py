import csv
import json
import os
import zipfile

from bson import ObjectId

import config
from visage.lib import Show
from visage.lib.campaign import model_campaign
from visage.lib.campaign.model_campaign import update_pending_table, get_all_campaigns, Campaign
from visage.lib.database_manager import prepare_data_json
from visage.lib.release.model_release import Release
from visage.lib.vote.model_vote import get_top_users
from visage_tools import fix_image


def midnight_script_update_pending_values():
    tops_users = get_top_users(3)

    # for each campaign, get number of images left

    for user in tops_users:
        update_user_values(user)

        print('updated', user)


def midnight_script_get_all_positive_files(directory_per_class, version, description, detailed):
    # get all campaigns

    create_zip_file = False
    insert_record = False
    file_data = {}
    strip_gps_data = True

    list_campaigns = model_campaign.get_all_campaigns()

    zip_extension = 'zip'
    # json_extension = 'json'
    csv_extension = 'csv'
    tsv_extension = 'tsv'

    ds_name = 'visage_v{}'.format(version)

    attachment_file = '{0}.{1}'.format(ds_name, zip_extension)
    metadata_filename = ds_name + '.{}'.format(csv_extension)
    complete_metadata_filename = ds_name + '.{}'.format(tsv_extension)

    zip_directory = os.path.expanduser(config.DS_PATH)
    zip_path = os.path.join(zip_directory, attachment_file)
    metadata_path = os.path.join(zip_directory, metadata_filename)
    complete_metadata_path = os.path.join(zip_directory, complete_metadata_filename)

    if not os.path.exists(zip_directory):
        os.mkdir(zip_directory)

    if True:
        if os.path.exists(metadata_path) or os.path.exists(zip_path) or os.path.exists(complete_metadata_path):
            print('file or metadata file already created, please check the dataset path ' + config.DS_PATH)
            return None

    with open(metadata_path, mode='w') as metadata_file:

        csv_writer = csv.DictWriter(metadata_file, fieldnames=['ucd_id', 'ucd_age', 'ucd_gender', 'fli_license',
                                                               'fli_user_name', 'fli_uploaded_date', 'fli_taken_date',
                                                               'original_height', 'original_width', 'ucd_detail'])
        csv_writer.writeheader()

    for c in list_campaigns:

        campaign_id = c.get('_id')

        campaign = Campaign(campaign_id=campaign_id)
        campaign.set_id()

        list_files = campaign.get_positive_files(votes=3, user_check=False)

        age = str(campaign.get_age())
        gender = campaign.get_gender()

        try:

            # write metadata

            for fi in list_files:

                license_name = real_name = user_name = uploaded_date = taken_date\
                    = json_column = original_width = original_height = ''

                with open(metadata_path, mode='a') as metadata_file:

                    csv_writer = csv.writer(metadata_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

                    # add flickr / azure data

                    json_column = prepare_data_json(fi)

                    flickr_db = json_column.get('flickr_db')

                    if flickr_db is None:

                        print('no flickr data recorded', fi)

                        # return None

                    else:
                        license = flickr_db.get('license')

                        if license is not None:
                            # do the mapping

                            dict_license = {
                                0: 'All Rights Reserved',
                                1: 'Attribution-NonCommercial-ShareAlike License',
                                2: 'Attribution-NonCommercial License',
                                3: 'Attribution-NonCommercial-NoDerivs License',
                                4: 'Attribution License',
                                5: 'Attribution-ShareAlike License',
                                6: 'Attribution-NoDerivs License',
                                7: 'No known copyright restrictions',
                                8: 'United States Government Work',
                                9: 'Public Domain Dedication (CC0)',
                                10: 'Public Domain Mark',
                            }

                            license_name = dict_license[int(license)]

                            if license_name.startswith('Attribution'):
                                # get username

                                owner = flickr_db.get('owner')

                                if owner is None:

                                    print('No owner for attribution', fi)

                                    return None
                                else:
                                    user_name = owner.get('username')

                                    if user_name is not None:
                                        user_name = user_name.encode('utf-8').strip()

                                    real_name = owner.get('realname')

                        uploaded_date = flickr_db.get('dateuploaded')

                        dates = flickr_db.get('dates')

                        if dates is None:
                            taken_date = ''
                        else:
                            taken_date = dates.get('taken')

                        original_height = flickr_db.get('height_o')
                        original_width = flickr_db.get('width_o')

                    if detailed:

                        csv_writer.writerow(
                            [os.path.splitext(fi)[0], age, gender, license_name, user_name,
                             uploaded_date, taken_date, original_height, original_width, json.dumps(json_column)])
                    else:
                        csv_writer.writerow([os.path.splitext(fi)[0], age, gender, license_name, user_name,
                                             uploaded_date, taken_date])

                    file_data[os.path.splitext(fi)[0]] = {
                        'file': fi,
                        'age': age,
                        'gender': gender
                    }

                # create csv out of dictionary

        except Exception as ex:
            print(ex)

        if create_zip_file:

            with zipfile.ZipFile(zip_path, "a", zipfile.ZIP_DEFLATED, allowZip64=True) as zf:

                print({'campaign': campaign_id, 'age': campaign.get_age(), 'gender': campaign.get_gender()})

                for f in list_files:

                    # do the automatic process to get the highest resolution image

                    if config.FIX_IMAGES:
                        fix_image(f)

                    if strip_gps_data:
                        pass

                    # copy files to temp folder

                    image_path = os.path.join(config.ALBUMS_DIR, age, f)

                    if os.path.exists(image_path):

                        if directory_per_class:
                            zf.write(image_path, os.path.join(age, str(f)))
                        else:
                            zf.write(image_path, str(f))

                    else:
                        print('path not available')

    print('zipped file created')

    # insert metadata into collection table

    # release
    # name, version, date
    # hash

    print('calculating hash ....')

    if insert_record:

        r = Release(version=version, file_path=zip_path, description=description, data=file_data)

        if r.insert():
            print('release copied successfully')


def update_user_values(user):
    dict_array = []

    service_method = 'azure'

    campaign_list = get_all_campaigns()

    for c in campaign_list:
        campaign_id = c.get('_id')

        campaign = Campaign(campaign_id=campaign_id)

        # get from collection that updates every midnight

        show = Show(user=str(user), campaign=campaign)
        show.set_method(service_method)

        number_images = show.nr_of_items_db

        dict_array.append({
            'campaign_id': ObjectId(campaign_id),
            'number_images': number_images,
            'user': user

        })

    update_pending_table(dict_array, user=user)


if __name__ == '__main__':

    if 1 == 1:
        midnight_script_get_all_positive_files(
            directory_per_class=True,
            version='1.3b',
            description='1.3 beta version',
            detailed=True
        )

    if 1 == 0:
        midnight_script_update_pending_values()
