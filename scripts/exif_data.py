import os
import shutil
from GPSPhoto import gpsphoto
from PIL import Image
from PIL.ExifTags import TAGS
import config

# change version accordingly###
# don't forget to change the library too

'''
modify piexif/_dump.py

near line 273

if isinstance(raw_value, tuple):
    value_str = ",".join([str(v) for v in raw_value]).encode("ASCII") + b"\x00" * (4 - length)
else:
    value_str = raw_value + b"\x00" * (4 - length)


'''

version = 'visage_v1.0b'
create_copy = False

###############################

def strip_gps_exif_data(image_path_input, image_path_output, output_metadata_directory):
    photo = gpsphoto.GPSPhoto(image_path_input)

    if photo._GPSPhoto__foundGPS:
        print('GPS data found .... file created', image_path_output)
        print(photo.getGPSData())

        # print metadata to file

        filename_without_extension = os.path.splitext(os.path.basename(image_path_input))[0]

        with open(os.path.join(output_metadata_directory, filename_without_extension + '.txt'), 'w') as my_file:
            for (k, v) in Image.open(image_path_input)._getexif().iteritems():
                exif_line = '%s = %s' % (TAGS.get(k), v)
                my_file.write(exif_line + '\n')

        photo.stripData(os.path.expanduser(image_path_output))
    else:

        if create_copy:
            shutil.copy(image_path_input, image_path_output)


def get_list_of_files(dir_name):
    # create a list of file and sub directories
    # names in the given directory
    list_of_file = os.listdir(dir_name)
    all_files = list()
    # Iterate over all the entries
    for entry in list_of_file:
        # Create full path

        if entry.startswith('.'):
            continue

        full_path = os.path.join(dir_name, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(full_path):
            all_files = all_files + get_list_of_files(full_path)
        else:
            all_files.append(full_path)

    return all_files


input_root_dir = os.path.expanduser(config.DS_PATH + '{}/'.format(version))
stripped_directory = os.path.expanduser(os.path.join(config.DS_PATH, 'stripped'))
output_root_dir = os.path.join(stripped_directory, version)
output_metadata = os.path.join(output_root_dir, 'metadata')

if not os.path.exists(stripped_directory):
    os.mkdir(stripped_directory)

if not os.path.exists(output_root_dir):
    os.mkdir(output_root_dir)

if not os.path.exists(output_root_dir):
    os.mkdir(output_root_dir)

if not os.path.exists(output_metadata):
    os.mkdir(output_metadata)

files = get_list_of_files(input_root_dir)

for f in files:

    folder = os.path.basename(os.path.dirname(f))
    filename = os.path.basename(f)

    if not os.path.exists(os.path.join(output_root_dir, folder)):
        os.mkdir(os.path.join(output_root_dir, folder))

    output_path = os.path.join(output_root_dir, folder, filename)

    strip_gps_exif_data(f, output_path, output_metadata)
