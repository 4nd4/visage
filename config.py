import os

DEBUG = True
TITLE = 'IMAGE DATABASE'
FRONTEND_HOST = '127.0.0.1'
FRONTEND_PORT = 5000
FRONTEND_BASEURL = 'http://photo'
FRONTEND_PREFIX = '/gallery'
FRONTEND_FCGI_SOCKET = '/var/lib/showoff/frontend.sock'
THUMBNAIL_SIZE = 150
GRID_SIZE = 200
THUMBNAILS_PER_PAGE = 30
THUMBNAILS_PER_SMALL_LIST = 8
FRONTEND_GRID_ITEMS_PER_PAGE = 50
IMAGE_SIZE = 800
ALLOWED_SIZES = [75, 150, 200, 400, 500, 640, 800, 1024, 1600, 'full']
CACHE_DIR = '/var/lib/visage/cache'
EDITS_DIR = '/var/lib/visage/edits'
SHOWS_DIR = '/var/lib/visage/shows'
ALBUMS_DIR = os.path.expanduser('~/Documents/images/')
ZIP_PATH = '~/Documents/images/zip/'
SECRET_KEY = 'SECRET_KEY'
FRONTEND_LIST_TEMPLATES = ['list', 'list_small', 'grid', 'galleria']
IMAGE_EXTENSION = '.jpg'
LATEST_CAMPAIGN_SIZE = 5
NUMBER_VOTES = 3
